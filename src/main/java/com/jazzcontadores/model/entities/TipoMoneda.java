/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Venegas
 */
@Entity
@Table(name = "tipomoneda", catalog = "jazzcontadores")
public class TipoMoneda implements java.io.Serializable {
    
    private Byte numero;
    private String descripcion;

    public TipoMoneda() {
    }

    public TipoMoneda(Byte numero, String descripcion) {
        this.numero = numero;
        this.descripcion = descripcion;
    }

    @Id
    @Column(name = "numero", unique = true, nullable = false)
    public Byte getNumero() {
        return numero;
    }

    public void setNumero(Byte numero) {
        this.numero = numero;
    }

    @Column(name = "descripcion", nullable = false, length = 95)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
