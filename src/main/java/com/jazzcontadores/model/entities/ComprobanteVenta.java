package com.jazzcontadores.model.entities;
// Generated 07/04/2013 11:07:12 PM by Hibernate Tools 3.2.1.GA

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Comprobanteventa generated by hbm2java
 */
@Entity
@Table(name = "comprobanteventa", catalog = "jazzcontadores", uniqueConstraints =
@UniqueConstraint(columnNames = {"idComprobanteVenta", "numero", "serie"}))
public class ComprobanteVenta implements java.io.Serializable {

    private Integer idComprobanteVenta;
    private TipoComprobantePagoODocumento tipoComprobantePagoODocumento;
    private Comprador comprador;
    private String numero;
    private String serie;
    private Date fechaEmision;
    private Date fechaVencimiento;
    private BigDecimal valorFacturadoExportacion;
    private BigDecimal baseImponibleOperacionGravada;
    private BigDecimal totalOperacionExonerada;
    private BigDecimal totalOperacionInafecta;
    private BigDecimal isc;
    private BigDecimal igv;
    private BigDecimal baseImponibleArrozPilado;
    private BigDecimal impuestoVentasArrozPilado;
    private BigDecimal otrosTributos;
    private BigDecimal importeTotal;
    private DetalleLibroRegistroVentas detalleLibroRegistroVentas;
    private DetalleLibroRegistroVentas detalleLibroRegistroVentasReferenciador;
    private List<DetalleComprobanteVenta> detallesComprobanteVenta = new ArrayList<DetalleComprobanteVenta>();

    public ComprobanteVenta() {
    }

    public ComprobanteVenta(TipoComprobantePagoODocumento tipocomprobantepagoodocumento, Comprador comprador, String numero, String serie, Date fechaEmision, BigDecimal base, BigDecimal igv, BigDecimal importeTotal) {
        this.tipoComprobantePagoODocumento = tipocomprobantepagoodocumento;
        this.comprador = comprador;
        this.numero = numero;
        this.serie = serie;
        this.fechaEmision = fechaEmision;
        this.baseImponibleOperacionGravada = base;
        this.igv = igv;
        this.importeTotal = importeTotal;
    }

    public ComprobanteVenta(TipoComprobantePagoODocumento tipocomprobantepagoodocumento, Comprador comprador, String numero, String serie, Date fechaEmision, Date fechaVencimiento, BigDecimal base, BigDecimal igv, BigDecimal importeTotal, List<DetalleComprobanteVenta> detallecomprobanteventas) {
        this.tipoComprobantePagoODocumento = tipocomprobantepagoodocumento;
        this.comprador = comprador;
        this.numero = numero;
        this.serie = serie;
        this.fechaEmision = fechaEmision;
        this.fechaVencimiento = fechaVencimiento;
        this.baseImponibleOperacionGravada = base;
        this.igv = igv;
        this.importeTotal = importeTotal;
        this.detallesComprobanteVenta = detallecomprobanteventas;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idComprobanteVenta", unique = true, nullable = false)
    public Integer getIdComprobanteVenta() {
        return this.idComprobanteVenta;
    }

    public void setIdComprobanteVenta(Integer idComprobanteVenta) {
        this.idComprobanteVenta = idComprobanteVenta;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "numeroTipoComprobante", nullable = false)
    public TipoComprobantePagoODocumento getTipoComprobantePagoODocumento() {
        return this.tipoComprobantePagoODocumento;
    }

    public void setTipoComprobantePagoODocumento(TipoComprobantePagoODocumento tipoComprobantePagoODocumento) {
        this.tipoComprobantePagoODocumento = tipoComprobantePagoODocumento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idComprador")
    public Comprador getComprador() {
        return this.comprador;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    @Column(name = "numero", nullable = false, length = 20)
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "serie", nullable = false, length = 20)
    public String getSerie() {
        return this.serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "fechaEmision", nullable = false, length = 10)
    public Date getFechaEmision() {
        return this.fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "fechaVencimiento", length = 10)
    public Date getFechaVencimiento() {
        return this.fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    @Column(name = "baseImponibleOperacionGravada", precision = 14, scale = 2)
    public BigDecimal getBaseImponibleOperacionGravada() {
        return this.baseImponibleOperacionGravada;
    }

    public void setBaseImponibleOperacionGravada(BigDecimal baseImponibleOperacionGravada) {
        this.baseImponibleOperacionGravada = baseImponibleOperacionGravada;
    }

    @Column(name = "igv", precision = 14, scale = 2)
    public BigDecimal getIgv() {
        return this.igv;
    }

    public void setIgv(BigDecimal igv) {
        this.igv = igv;
    }

    @Column(name = "importeTotal", nullable = false, precision = 14, scale = 2)
    public BigDecimal getImporteTotal() {
        return this.importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "comprobanteVenta")
    public DetalleLibroRegistroVentas getDetalleLibroRegistroVentas() {
        return detalleLibroRegistroVentas;
    }

    public void setDetalleLibroRegistroVentas(DetalleLibroRegistroVentas detalleLibroRegistroVentas) {
        this.detalleLibroRegistroVentas = detalleLibroRegistroVentas;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "comprobanteVentaReferenciado")
    public DetalleLibroRegistroVentas getDetalleLibroRegistroVentasReferenciador() {
        return detalleLibroRegistroVentasReferenciador;
    }

    public void setDetalleLibroRegistroVentasReferenciador(DetalleLibroRegistroVentas detalleLibroRegistroVentasReferenciador) {
        this.detalleLibroRegistroVentasReferenciador = detalleLibroRegistroVentasReferenciador;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "comprobanteVenta")
    public List<DetalleComprobanteVenta> getDetallesComprobanteVenta() {
        return this.detallesComprobanteVenta;
    }

    public void setDetallesComprobanteVenta(List<DetalleComprobanteVenta> detallesComprobanteVenta) {
        this.detallesComprobanteVenta = detallesComprobanteVenta;
    }

    @Column(name = "valorFacturadoExportacion", precision = 14, scale = 2)
    public BigDecimal getValorFacturadoExportacion() {
        return valorFacturadoExportacion;
    }

    public void setValorFacturadoExportacion(BigDecimal valorFacturadoExportacion) {
        this.valorFacturadoExportacion = valorFacturadoExportacion;
    }

    @Column(name = "totalOperacionExonerada", precision = 14, scale = 2)
    public BigDecimal getTotalOperacionExonerada() {
        return totalOperacionExonerada;
    }

    public void setTotalOperacionExonerada(BigDecimal totalOperacionExonerada) {
        this.totalOperacionExonerada = totalOperacionExonerada;
    }

    @Column(name = "totalOperacionInafecta", precision = 14, scale = 2)
    public BigDecimal getTotalOperacionInafecta() {
        return totalOperacionInafecta;
    }

    public void setTotalOperacionInafecta(BigDecimal totalOperacionInafecta) {
        this.totalOperacionInafecta = totalOperacionInafecta;
    }

    @Column(name = "isc", precision = 14, scale = 2)
    public BigDecimal getIsc() {
        return isc;
    }

    public void setIsc(BigDecimal isc) {
        this.isc = isc;
    }

    @Column(name = "baseImponibleArrozPilado", precision = 14, scale = 2)
    public BigDecimal getBaseImponibleArrozPilado() {
        return baseImponibleArrozPilado;
    }

    public void setBaseImponibleArrozPilado(BigDecimal baseImponibleArrozPilado) {
        this.baseImponibleArrozPilado = baseImponibleArrozPilado;
    }

    @Column(name = "impuestoVentasArrozPilado", precision = 14, scale = 2)
    public BigDecimal getImpuestoVentasArrozPilado() {
        return impuestoVentasArrozPilado;
    }

    public void setImpuestoVentasArrozPilado(BigDecimal impuestoVentasArrozPilado) {
        this.impuestoVentasArrozPilado = impuestoVentasArrozPilado;
    }

    @Column(name = "otrosTributos", precision = 14, scale = 2)
    public BigDecimal getOtrosTributos() {
        return otrosTributos;
    }

    public void setOtrosTributos(BigDecimal otrosTributos) {
        this.otrosTributos = otrosTributos;
    }
}
