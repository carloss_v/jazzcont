/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.model.dao;

import com.jazzcontadores.model.entities.RegistroCuentaContable;
import java.util.Date;

/**
 *
 * @author Venegas
 */
public interface RegistroCuentaContableDAO extends GenericDAO<RegistroCuentaContable, Integer> {

    RegistroCuentaContable findByPeriodo(long ruc, Date periodo, String numeroCuenta);
}
