/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.model.dao;

import com.jazzcontadores.model.entities.ComprobanteVenta;

/**
 *
 * @author Venegas
 */
public interface ComprobanteVentaDAO extends GenericDAO<ComprobanteVenta, Integer> {

    ComprobanteVenta findByEmpresaSerieNumero(Long ruc, String serie, String numero);
    
    ComprobanteVenta findByIdAndEmpresa(Long ruc, int idComp);
}
