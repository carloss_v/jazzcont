/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.model.daoimpl;

import com.jazzcontadores.model.dao.RegistroCuentaContableDAO;
import com.jazzcontadores.model.entities.RegistroCuentaContable;
import java.util.Date;

/**
 *
 * @author Venegas
 */
public class RegistroCuentaContableDAOImpl extends GenericDAOImpl<RegistroCuentaContable, Integer>
        implements RegistroCuentaContableDAO {

    @Override
    public RegistroCuentaContable findByPeriodo(long ruc, Date periodo, String numeroCuenta) {
        RegistroCuentaContable rcc;

        rcc = (RegistroCuentaContable) getSession().createQuery("select distinct rcc "
                + "from RegistroCuentaContable rcc "
                + "join rcc.detallesCargos dc "                
                + "join dc.asientoContable ac "                
                + "join ac.libroDiarioSimplificado l "           
                + "join l.empresaCliente e "               
                + "where e.ruc = :ruc "               
                + "and rcc.mes = :periodo "
                + "and rcc.cuentaContable.numero = :numeroCuenta")
                .setLong("ruc", ruc)
                .setDate("periodo", periodo)
                .setString("numeroCuenta", numeroCuenta)
                .uniqueResult();
        
        if (rcc == null) {
            rcc = (RegistroCuentaContable) getSession().createQuery("select distinct rcc "
                + "from RegistroCuentaContable rcc "
                + "join rcc.detallesAbonos da "                
                + "join da.asientoContable ac "                
                + "join ac.libroDiarioSimplificado l "           
                + "join l.empresaCliente e "               
                + "where e.ruc = :ruc "               
                + "and rcc.mes = :periodo "
                + "and rcc.cuentaContable.numero = :numeroCuenta")
                .setLong("ruc", ruc)
                .setDate("periodo", periodo)
                .setString("numeroCuenta", numeroCuenta)
                .uniqueResult();
        }

        return rcc;
    }
}
