/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.model.daoimpl;

import com.jazzcontadores.model.dao.ComprobanteCompraDAO;
import com.jazzcontadores.model.entities.ComprobanteCompra;

/**
 *
 * @author Venegas
 */
public class ComprobanteCompraDAOImpl extends GenericDAOImpl<ComprobanteCompra, Integer>
        implements ComprobanteCompraDAO {

    @Override
    public ComprobanteCompra findByEmpresaSerieNumero(Long ruc, String serie, String numero) {
        ComprobanteCompra comprobante;

        comprobante = (ComprobanteCompra) getSession().createQuery("from ComprobanteCompra c "
                + "where c.detalleLibroRegistroCompras.libroRegistroCompras.empresaCliente.ruc = :ruc "
                + "and c.serie = :serie "
                + "and c.numero = :numero")
                .setLong("ruc", ruc)
                .setString("serie", serie)
                .setString("numero", numero)
                .uniqueResult();

        return comprobante;
    }

    @Override
    public ComprobanteCompra findByIdAndEmpresa(Long ruc, int idComp) {
        ComprobanteCompra comprobante;

        comprobante = (ComprobanteCompra) getSession().createQuery("from ComprobanteCompra c "
                + "where c.detalleLibroRegistroCompras.libroRegistroCompras.empresaCliente.ruc = :ruc "
                + "and c.id = :idComp")
                .setLong("ruc", ruc)
                .setInteger("idComp", idComp)
                .uniqueResult();

        return comprobante;
    }
}
