/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.model.daoimpl;

import com.jazzcontadores.model.dao.ContactoDAO;
import com.jazzcontadores.model.entities.Contacto;
import java.util.List;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Venegas
 */
public class ContactoDAOImpl extends GenericDAOImpl<Contacto, Integer>
        implements ContactoDAO {

    @Override
    public Contacto findByEmail(String email) {
        Contacto contacto;

        contacto = (Contacto) getSession().createQuery("from Contacto c "
                + "where c.emailPrincipal = :email")
                .setString("email", email)
                .uniqueResult();

        return contacto;
    }

    @Override
    public boolean sendMessage(List<String> destinatarios, String tema, String mensaje) {

        final String username = "carlos.vj.011@gmail.com";
        final String password = "01382155144";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {

                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));

            Address[] adresses = new Address[destinatarios.size()];

            for (int i = 0; i < destinatarios.size(); i++) {
                adresses[i] = new InternetAddress(destinatarios.get(i));
            }

            message.setRecipients(Message.RecipientType.TO, adresses);
            message.setSubject(tema);
            message.setText(mensaje);

            Transport.send(message);

            return true;

        } catch (MessagingException e) {
            System.err.println(e + ": " + e.getMessage());
            return false;
        }
    }
}
