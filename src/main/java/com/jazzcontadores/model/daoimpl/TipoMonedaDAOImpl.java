/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.model.daoimpl;

import com.jazzcontadores.model.dao.TipoMonedaDAO;
import com.jazzcontadores.model.entities.TipoMoneda;

/**
 *
 * @author Venegas
 */
public class TipoMonedaDAOImpl extends GenericDAOImpl<TipoMoneda, Byte>
        implements TipoMonedaDAO {
}
