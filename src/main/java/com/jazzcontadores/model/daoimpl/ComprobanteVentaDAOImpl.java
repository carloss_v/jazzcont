/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.model.daoimpl;

import com.jazzcontadores.model.dao.ComprobanteVentaDAO;
import com.jazzcontadores.model.entities.ComprobanteVenta;

/**
 *
 * @author Venegas
 */
public class ComprobanteVentaDAOImpl extends GenericDAOImpl<ComprobanteVenta, Integer>
        implements ComprobanteVentaDAO {
    
    @Override
    public ComprobanteVenta findByEmpresaSerieNumero(Long ruc, String serie, String numero) {
        ComprobanteVenta comprobante;
        
        comprobante = (ComprobanteVenta) getSession().createQuery("from ComprobanteVenta c "
                + "where c.detalleLibroRegistroVentas.libroRegistroVentas.empresaCliente.ruc = :ruc "
                + "and c.serie = :serie "
                + "and c.numero = :numero")
                .setLong("ruc", ruc)
                .setString("serie", serie)
                .setString("numero", numero)
                .uniqueResult();
        
        return comprobante;
    }
    
    @Override
    public ComprobanteVenta findByIdAndEmpresa(Long ruc, int idComp) {
        ComprobanteVenta comprobante;
        
        comprobante = (ComprobanteVenta) getSession().createQuery("from ComprobanteVenta c "
                + "where c.detalleLibroRegistroVentas.libroRegistroVentas.empresaCliente.ruc = :ruc "
                + "and c.id = :idComp")
                .setLong("ruc", ruc)
                .setInteger("idComp", idComp)
                .uniqueResult();
        
        return comprobante;
    }
}
