/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.controller.admin;

import com.jazzcontadores.model.dao.EmpresaClienteDAO;
import com.jazzcontadores.model.entities.AsientoContable;
import com.jazzcontadores.model.entities.DetalleAbono;
import com.jazzcontadores.model.entities.DetalleCargo;
import com.jazzcontadores.model.entities.EmpresaCliente;
import com.jazzcontadores.model.entities.LibroDiarioSimplificado;
import com.jazzcontadores.model.entities.RegistroCuentaContable;
import com.jazzcontadores.model.entities.TipoMoneda;
import com.jazzcontadores.util.DAOFactory;
import com.jazzcontadores.util.HibernateUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Venegas
 */
public class AsientoAction extends ActionSupport {
    
    private long ruc;
    private EmpresaCliente empresaCliente;
    private AsientoContable asiento;
    
    public String add() {
        
        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();
        
        EmpresaCliente e = empresaDAO.findByRuc(ruc);
        
        if (e != null) {
            this.setEmpresaCliente(e);
        } else {
            return ERROR;
        }
        
        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "add";
    }
    
    public String save() throws Exception {
        
        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        
        Calendar cPeriodo = new GregorianCalendar();
        cPeriodo.setTime(this.getAsiento().getFecha());
        // para los periodos de los libros, el día es 1 siempre
        cPeriodo.set(Calendar.DAY_OF_MONTH, 1);
        Calendar cFechaFin = (Calendar) cPeriodo.clone();
        cFechaFin.set(Calendar.DAY_OF_MONTH, cFechaFin.getActualMaximum(Calendar.DAY_OF_MONTH));
        
        LibroDiarioSimplificado libroExistente =
                factory.getLibroDiarioSimplificadoDAO().findByPeriodo(this.getEmpresaCliente().getRuc(), cPeriodo.getTime());
        
        try {
            if (libroExistente == null) {
                
                LibroDiarioSimplificado libroDSNuevo = new LibroDiarioSimplificado();
                libroDSNuevo.setPeriodo(cPeriodo.getTime());
                libroDSNuevo.setFechaInicio(cPeriodo.getTime());
                libroDSNuevo.setFechaFin(cFechaFin.getTime());
                libroDSNuevo.setEstaCerrado(false);
                libroDSNuevo.setEmpresaCliente(this.getEmpresaCliente());
                
                factory.getLibroDiarioSimplificadoDAO().makePersistent(libroDSNuevo);
                
                this.getAsiento().setLibroDiarioSimplificado(libroDSNuevo);
                this.getAsiento().setFechaHoraRegistro(new Date());
                this.getAsiento().setNumeroCorrelativo(1); // libro nuevo, primer detalle


                // establecemos los registro cuentas contables
                for (DetalleAbono d : this.getAsiento().getDetallesAbono()) {
                    // si el libro diario simplificado es nuevo, no existe un registro de cuenta contable del periodo
                    RegistroCuentaContable rcc = new RegistroCuentaContable();
                    rcc.setMes(cPeriodo.getTime());
                    rcc.setFechaInicio(cPeriodo.getTime());
                    rcc.setFechaFin(cFechaFin.getTime());
                    rcc.setEstaSaldada(false);
                    rcc.setEstaCerrada(false);
                    rcc.setCuentaContable(factory.getCuentaContableDAO().findById(d.getRegistroCuentaContable().getCuentaContable().getNumero()));
                    factory.getRegistroCuentaContableDAO().makePersistent(rcc);
                    d.setRegistroCuentaContable(rcc);
                }
                
                for (DetalleCargo d : this.getAsiento().getDetallesCargo()) {
                    // si el libro diario simplificado es nuevo, no existe un registro de cuenta contable del periodo
                    RegistroCuentaContable rcc = new RegistroCuentaContable();
                    rcc.setMes(cPeriodo.getTime());
                    rcc.setFechaInicio(cPeriodo.getTime());
                    rcc.setFechaFin(cFechaFin.getTime());
                    rcc.setEstaSaldada(false);
                    rcc.setEstaCerrada(false);
                    rcc.setCuentaContable(factory.getCuentaContableDAO().findById(d.getRegistroCuentaContable().getCuentaContable().getNumero()));
                    factory.getRegistroCuentaContableDAO().makePersistent(rcc);
                    d.setRegistroCuentaContable(rcc);
                }
                
                libroDSNuevo.getAsientosContables().add(this.getAsiento());
                
            } else {
                if (libroExistente.isEstaCerrado()) {
                    return "libroCerrado";
                }
                
                this.getAsiento().setLibroDiarioSimplificado(libroExistente);
                this.getAsiento().setFechaHoraRegistro(new Date());

                // establecemos los registros de cuentas contables
                for (DetalleAbono d : this.getAsiento().getDetallesAbono()) {
                    RegistroCuentaContable rcc = factory.getRegistroCuentaContableDAO()
                            .findByPeriodo(this.getEmpresaCliente().getRuc(), cPeriodo.getTime(), d.getRegistroCuentaContable().getCuentaContable().getNumero());
                    if (rcc == null) {
                        RegistroCuentaContable r = new RegistroCuentaContable();
                        r.setMes(cPeriodo.getTime());
                        r.setFechaInicio(cPeriodo.getTime());
                        r.setFechaFin(cFechaFin.getTime());
                        r.setEstaSaldada(false);
                        r.setEstaCerrada(false);
                        r.setCuentaContable(factory.getCuentaContableDAO().findById(d.getRegistroCuentaContable().getCuentaContable().getNumero()));
                        factory.getRegistroCuentaContableDAO().makePersistent(r);
                        d.setRegistroCuentaContable(r);                        
                    } else {
                        d.setRegistroCuentaContable(rcc);
                    }
                }
                
                for (DetalleCargo d : this.getAsiento().getDetallesCargo()) {
                    RegistroCuentaContable rcc = factory.getRegistroCuentaContableDAO()
                            .findByPeriodo(this.getEmpresaCliente().getRuc(), cPeriodo.getTime(), d.getRegistroCuentaContable().getCuentaContable().getNumero());
                    if (rcc == null) {
                        RegistroCuentaContable r = new RegistroCuentaContable();
                        r.setMes(cPeriodo.getTime());
                        r.setFechaInicio(cPeriodo.getTime());
                        r.setFechaFin(cFechaFin.getTime());
                        r.setEstaSaldada(false);
                        r.setEstaCerrada(false);
                        r.setCuentaContable(factory.getCuentaContableDAO().findById(d.getRegistroCuentaContable().getCuentaContable().getNumero()));
                        factory.getRegistroCuentaContableDAO().makePersistent(r);
                        d.setRegistroCuentaContable(r);
                    } else {
                        d.setRegistroCuentaContable(rcc);
                    }
                }


                // reordenamos los asientos

                if (libroExistente.getAsientosContables().size() > 0) {
                    Date ultimaFechaRegistrada = libroExistente.getAsientosContables()
                            .get(libroExistente.getAsientosContables().size() - 1) // esto da error cuando el libro existe pero no tiene asientos *************************
                            .getFecha();
                    // si la fecha del asiento es anterior a la fecha del último asiento del libro
                    if (this.getAsiento().getFecha().before(ultimaFechaRegistrada)) {
                        libroExistente.getAsientosContables().add(this.getAsiento());

                        // reordenamos la lista
                        Collections.sort(libroExistente.getAsientosContables(), new Comparator<AsientoContable>() {
                            @Override
                            public int compare(AsientoContable a1, AsientoContable a2) {
                                return a1.getFecha().compareTo(a2.getFecha());
                            }
                        });
                        // establecemos el numero correlativo
                        for (int i = 0; i < libroExistente.getAsientosContables().size(); i++) {
                            // se reordena la lista de detalles
                            libroExistente.getAsientosContables().get(i).setNumeroCorrelativo(i + 1);
                        }
                    } else {
                        // le asignamos el último número correlativo más 1
                        AsientoContable a = libroExistente.getAsientosContables().get(libroExistente.getAsientosContables().size() - 1);
                        int ultimoNCorrelativo = a.getNumeroCorrelativo();
                        this.getAsiento().setNumeroCorrelativo(ultimoNCorrelativo + 1);
                    }
                } else {
                    this.getAsiento().setNumeroCorrelativo(1); // primer asiento
                }
                
                libroExistente.getAsientosContables().add(this.getAsiento());
                
            }
            
            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
            
        } catch (Exception e) {
            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().rollback();
            System.err.println(e + ": " + e.getMessage());
            throw e;
            //return ERROR;
        }
        
        return "save";
    }
    
    public void validateSave() {
        // se inyecta el cliente antes de hacer las validaciones
        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        EmpresaCliente e = factory.getEmpresaClienteDAO().findByRuc(ruc);
        this.setEmpresaCliente(e);


        // validaciones

        if (this.getAsiento().getFecha() != null) {
            if (getAsiento().getFecha().after(new Date())) {
                addActionError("No se puede registrar fechas futuras en el sistema.");
            }
        } else {
            addActionError("Debe especificar la fecha del asiento.");
        }
        
        if (this.getAsiento().getGlosa().trim().equals("")) {
            addActionError("Debe especificar la glosa del asiento.");
        }
        
        if (this.getAsiento().getDetallesAbono().isEmpty()) {
            addActionError("Debe especificar las cuentas que se abonan.");
        }
        
        if (this.getAsiento().getDetallesCargo().isEmpty()) {
            addActionError("Debe especificar las cuentas que se cargan.");
        }
        
        BigDecimal totalCargo = new BigDecimal("0.00");
        BigDecimal totalAbono = new BigDecimal("0.00");
        totalCargo = totalCargo.setScale(2, RoundingMode.HALF_EVEN);
        totalAbono = totalAbono.setScale(2, RoundingMode.HALF_EVEN);
        TipoMoneda tm = factory.getTipoMonedaDAO().findById((byte) 1);
        
        for (DetalleCargo detalleCargo : this.getAsiento().getDetallesCargo()) {
            totalCargo = totalCargo.add(detalleCargo.getImporte());
            detalleCargo.setAsientoContable(this.getAsiento());
            detalleCargo.setTipoMoneda(tm);
        }
        
        for (DetalleAbono detalleAbono : this.getAsiento().getDetallesAbono()) {
            totalAbono = totalAbono.add(detalleAbono.getImporte());
            detalleAbono.setAsientoContable(this.getAsiento());
            detalleAbono.setTipoMoneda(tm);
        }
        
        if (totalCargo.compareTo(totalAbono) != 0) {
            addActionError("El total de cargos no coincide con el total de abonos.");
        }
        
        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
    }
    
    public long getRuc() {
        return ruc;
    }
    
    public void setRuc(long ruc) {
        this.ruc = ruc;
    }
    
    public EmpresaCliente getEmpresaCliente() {
        return empresaCliente;
    }
    
    public void setEmpresaCliente(EmpresaCliente empresaCliente) {
        this.empresaCliente = empresaCliente;
    }
    
    public AsientoContable getAsiento() {
        return asiento;
    }
    
    public void setAsiento(AsientoContable asiento) {
        this.asiento = asiento;
    }
}
