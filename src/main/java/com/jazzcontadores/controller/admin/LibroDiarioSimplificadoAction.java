/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.controller.admin;

import com.jazzcontadores.model.dao.EmpresaClienteDAO;
import com.jazzcontadores.model.entities.AsientoContable;
import com.jazzcontadores.model.entities.CuentaContable;
import com.jazzcontadores.model.entities.DetalleAbono;
import com.jazzcontadores.model.entities.DetalleCargo;
import com.jazzcontadores.model.entities.EmpresaCliente;
import com.jazzcontadores.model.entities.LibroDiarioSimplificado;
import com.jazzcontadores.util.DAOFactory;
import com.jazzcontadores.util.HibernateUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Venegas
 */
public class LibroDiarioSimplificadoAction extends ActionSupport {

    private long ruc;
    private int idLibro;
    private LibroDiarioSimplificado lds;
    private EmpresaCliente empresaCliente;
    private InputStream fileInputStream;

    public String show() {
        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();

        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        EmpresaCliente e = empresaDAO.findByRuc(this.getRuc());

        if (e != null) {
            this.setEmpresaCliente(e);
        } else {
            return ERROR;
        }

        LibroDiarioSimplificado libro = factory.getLibroDiarioSimplificadoDAO().findByIdAndEmpresa(this.getRuc(), this.getIdLibro());
        this.setLds(libro);

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "show";
    }

    public String downloadAsExcel() throws Exception {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        LibroDiarioSimplificado libro = factory.getLibroDiarioSimplificadoDAO().findByIdAndEmpresa(this.getRuc(), this.getIdLibro());
        this.setLds(libro);

        // obtener todas las cuentas implicadas en los asientos del libro
        List<CuentaContable> cuentas = new ArrayList<CuentaContable>();
        for (AsientoContable a : this.getLds().getAsientosContables()) {
            for (DetalleCargo dc : a.getDetallesCargo()) {
                if (!cuentas.contains(dc.getRegistroCuentaContable().getCuentaContable())) {
                    cuentas.add(dc.getRegistroCuentaContable().getCuentaContable());
                }
            }
            for (DetalleAbono da : a.getDetallesAbono()) {
                if (!cuentas.contains(da.getRegistroCuentaContable().getCuentaContable())) {
                    cuentas.add(da.getRegistroCuentaContable().getCuentaContable());
                }
            }
        }

        // ordenamos la lista ascendentemente por el numero de cuenta
        Collections.sort(cuentas, new Comparator<CuentaContable>() {
            @Override
            public int compare(CuentaContable c1, CuentaContable c2) {
                return c1.getNumero().compareTo(c2.getNumero());
            }
        });

        // arreglos de tipos de cuentas
        List<String> activos = new ArrayList<String>();
        List<String> pasivos = new ArrayList<String>();
        List<String> patrimonio = new ArrayList<String>();
        List<String> gastos = new ArrayList<String>();
        List<String> ingresos = new ArrayList<String>();
        List<String> saldosIntermediarios = new ArrayList<String>();
        List<String> cuentasFuncionGasto = new ArrayList<String>();
        List<String> cuentasOrden = new ArrayList<String>();

        for (CuentaContable c : cuentas) {
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 0) {
                cuentasOrden.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 1
                    || c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 2
                    || c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 3) {
                activos.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 4) {
                pasivos.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 5) {
                patrimonio.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 6) {
                gastos.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 7) {
                ingresos.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 8) {
                saldosIntermediarios.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 9) {
                cuentasFuncionGasto.add(c.getNumero());
            }
        }

        // Apache POI

        Workbook wb = new XSSFWorkbook();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
        Sheet sheet1 = wb.createSheet(sdf.format(this.getLds().getPeriodo()));
        sheet1.setColumnWidth(0, 3072); // num. correlativo
        sheet1.setColumnWidth(1, 3584); // fecha
        sheet1.setColumnWidth(2, 10240); // glosa

        // estilos para las celdas
        Font font = wb.createFont(); // font = negrita y tamaño 15
        font.setFontHeightInPoints((short) 15);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);

        Font font2 = wb.createFont(); // font2 = negrita solo              
        font2.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);

        CellStyle style1 = wb.createCellStyle();
        style1.setFont(font);

        CellStyle style2 = wb.createCellStyle();
        style2.setFont(font2);

        CellStyle style3 = wb.createCellStyle(); // estilo para los encabezados de la tabla
        style3.setFont(font2);
        style3.setWrapText(true);
        style3.setAlignment(CellStyle.ALIGN_CENTER);
        style3.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style3.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style3.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style3.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style3.setBorderLeft(XSSFCellStyle.BORDER_THIN);

        CellStyle style4 = wb.createCellStyle(); // estilo para las celdas de la tabla
        style4.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style4.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style4.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style4.setBorderLeft(XSSFCellStyle.BORDER_THIN);

        CellStyle style5 = wb.createCellStyle(); // con border y negrita
        style5.setFont(font2);
        style5.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style5.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style5.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style5.setBorderLeft(XSSFCellStyle.BORDER_THIN);

        // cabecera del libro
        Row row1 = sheet1.createRow((short) 0);
        Cell c0r1 = row1.createCell(0);
        c0r1.setCellValue("FORMATO 5.2: LIBRO DIARIO DE FORMATO SIMPLIFICADO");
        c0r1.setCellStyle(style1);

        Row row2 = sheet1.createRow((short) 2);
        Cell c0r2 = row2.createCell(0);
        c0r2.setCellValue("PERIODO:");
        c0r2.setCellStyle(style2);

        Cell c1r2 = row2.createCell(1);
        c1r2.setCellValue(sdf.format(this.getLds().getPeriodo()));

        Row row3 = sheet1.createRow((short) 3);
        Cell c0r3 = row3.createCell(0);
        c0r3.setCellValue("RUC:");
        c0r3.setCellStyle(style2);

        Cell c1r3 = row3.createCell(1);
        c1r3.setCellValue(Long.toString(this.getLds().getEmpresaCliente().getRuc())); // para q no lo tome como long

        Row row4 = sheet1.createRow((short) 4);
        Cell c0r4 = row4.createCell(0);
        c0r4.setCellValue("APELLIDOS Y NOMBRES, DENOMINACIÓN O RAZÓN SOCIAL:");
        c0r4.setCellStyle(style2);

        row4.createCell(3).setCellValue(this.getLds().getEmpresaCliente().getRazonSocial());

        // encabezado de la tabla
        Row row5 = sheet1.createRow((short) 6);
        Row row6 = sheet1.createRow((short) 7);

        int nMontos = activos.size() + pasivos.size() + patrimonio.size() + gastos.size()
                + ingresos.size() + saldosIntermediarios.size() + cuentasFuncionGasto.size() + cuentasOrden.size();

        // creamos el encabezado de la tabla
        for (int i = 0; i < (nMontos + 3); i++) {
            Cell cell = row5.createCell(i);
            cell.setCellStyle(style3);
        }
        for (int i = 0; i < (nMontos + 3); i++) {
            Cell cell = row6.createCell(i);
            cell.setCellStyle(style3);
        }

        row5.getCell(0).setCellValue("Número correlativo o Código Único de la Operación");
        row5.getCell(1).setCellValue("Fecha o período de la Operación");
        row5.getCell(2).setCellValue("Glosa o descripción de la Operación");

        int i = 3;
        for (String s : activos) {
            row6.getCell(i++).setCellValue(s);
        }
        for (String s : pasivos) {
            row6.getCell(i++).setCellValue(s);
        }
        for (String s : patrimonio) {
            row6.getCell(i++).setCellValue(s);
        }
        for (String s : gastos) {
            row6.getCell(i++).setCellValue(s);
        }
        for (String s : ingresos) {
            row6.getCell(i++).setCellValue(s);
        }
        for (String s : saldosIntermediarios) {
            row6.getCell(i++).setCellValue(s);
        }
        for (String s : cuentasFuncionGasto) {
            row6.getCell(i++).setCellValue(s);
        }
        for (String s : cuentasOrden) {
            row6.getCell(i++).setCellValue(s);
        }



        sheet1.addMergedRegion(new CellRangeAddress(4, 4, 0, 2)); // razon social
        sheet1.addMergedRegion(new CellRangeAddress(6, 7, 0, 0)); // num. correlativo
        sheet1.addMergedRegion(new CellRangeAddress(6, 7, 1, 1)); // fecha operacion
        sheet1.addMergedRegion(new CellRangeAddress(6, 7, 2, 2)); // glosa
        int j = 3;
        if (!activos.isEmpty()) {
            row5.getCell(j).setCellValue("ACTIVOS");
            sheet1.addMergedRegion(new CellRangeAddress(6, 6, j, activos.size() + 2));
            j += activos.size();
        }
        if (!pasivos.isEmpty()) {
            row5.getCell(j).setCellValue("PASIVOS");
            sheet1.addMergedRegion(new CellRangeAddress(6, 6, j, j + pasivos.size() - 1));
            j += pasivos.size();
        }
        if (!patrimonio.isEmpty()) {
            row5.getCell(j).setCellValue("PATRIMONIO");
            sheet1.addMergedRegion(new CellRangeAddress(6, 6, j, j + patrimonio.size() - 1));
            j += patrimonio.size();
        }
        if (!gastos.isEmpty()) {
            row5.getCell(j).setCellValue("GASTOS");
            sheet1.addMergedRegion(new CellRangeAddress(6, 6, j, j + gastos.size() - 1));
            j += gastos.size();
        }
        if (!ingresos.isEmpty()) {
            row5.getCell(j).setCellValue("INGRESOS");
            sheet1.addMergedRegion(new CellRangeAddress(6, 6, j, j + ingresos.size() - 1));
            j += ingresos.size();
        }
        if (!saldosIntermediarios.isEmpty()) {
            row5.getCell(j).setCellValue("SALDOS INTERMEDIARIOS DE GESTIÓN");
            sheet1.addMergedRegion(new CellRangeAddress(6, 6, j, j + saldosIntermediarios.size() - 1));
            j += saldosIntermediarios.size();
        }
        if (!cuentasFuncionGasto.isEmpty()) {
            row5.getCell(j).setCellValue("CUENTAS DE FUNCIÓN DEL GASTO");
            sheet1.addMergedRegion(new CellRangeAddress(6, 6, j, j + cuentasFuncionGasto.size() - 1));
            j += cuentasFuncionGasto.size();
        }
        if (!cuentasOrden.isEmpty()) {
            row5.getCell(j).setCellValue("CUENTAS DE ORDEN");
            sheet1.addMergedRegion(new CellRangeAddress(6, 6, j, j + cuentasOrden.size() - 1));
        }


        // registros de la tabla
        int c = 8; // contador de filas
        // arreglo de totales por columnas
        List<BigDecimal> totales = new ArrayList<BigDecimal>();
        for (int m = 0; m < nMontos; m++) {
            BigDecimal t = new BigDecimal("0.00");
            t = t.setScale(2, RoundingMode.HALF_EVEN);
            totales.add(t);
        }
        
        for (AsientoContable a : this.getLds().getAsientosContables()) {

            Row r = sheet1.createRow(c++);
            for (int k = 0; k < nMontos + 3; k++) {
                Cell cell = r.createCell(k);
                cell.setCellStyle(style4);
            }

            r.getCell(0).setCellValue(a.getNumeroCorrelativo());
            r.getCell(1).setCellValue(a.getFecha());
            r.getCell(2).setCellValue(a.getGlosa());

            List detalles = new ArrayList();
            detalles.addAll(a.getDetallesAbono());
            detalles.addAll(a.getDetallesCargo());
            
            
            for (Object o : detalles) {
                int m = 3;
                for (String s : activos) {                    
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleCargo) o).getImporte()));  
                        r.getCell(m++).setCellValue(((DetalleCargo) o).getImporte().doubleValue());                                              
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleAbono) o).getImporte().negate())); 
                        r.getCell(m++).setCellValue(((DetalleAbono) o).getImporte().negate().doubleValue());                        
                    } else {
                        m++;
                    }
                }
                for (String s : pasivos) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleCargo) o).getImporte()));  
                        r.getCell(m++).setCellValue(((DetalleCargo) o).getImporte().doubleValue());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleAbono) o).getImporte().negate())); 
                        r.getCell(m++).setCellValue(((DetalleAbono) o).getImporte().negate().doubleValue());
                    } else {
                        m++;
                    }
                }
                for (String s : patrimonio) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleCargo) o).getImporte()));
                        r.getCell(m++).setCellValue(((DetalleCargo) o).getImporte().doubleValue());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleAbono) o).getImporte().negate())); 
                        r.getCell(m++).setCellValue(((DetalleAbono) o).getImporte().negate().doubleValue());
                    } else {
                        m++;
                    }
                }
                for (String s : gastos) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleCargo) o).getImporte()));
                        r.getCell(m++).setCellValue(((DetalleCargo) o).getImporte().doubleValue());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleAbono) o).getImporte().negate())); 
                        r.getCell(m++).setCellValue(((DetalleAbono) o).getImporte().negate().doubleValue());
                    } else {
                        m++;
                    }
                }
                for (String s : ingresos) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleCargo) o).getImporte()));
                        r.getCell(m++).setCellValue(((DetalleCargo) o).getImporte().doubleValue());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleAbono) o).getImporte().negate())); 
                        r.getCell(m++).setCellValue(((DetalleAbono) o).getImporte().negate().doubleValue());
                    } else {
                        m++;
                    }
                }
                for (String s : saldosIntermediarios) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleCargo) o).getImporte()));
                        r.getCell(m++).setCellValue(((DetalleCargo) o).getImporte().doubleValue());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleAbono) o).getImporte().negate())); 
                        r.getCell(m++).setCellValue(((DetalleAbono) o).getImporte().negate().doubleValue());
                    } else {
                        m++;
                    }
                }
                for (String s : cuentasFuncionGasto) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleCargo) o).getImporte()));
                        r.getCell(m++).setCellValue(((DetalleCargo) o).getImporte().doubleValue());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleAbono) o).getImporte().negate())); 
                        r.getCell(m++).setCellValue(((DetalleAbono) o).getImporte().negate().doubleValue());
                    } else {
                        m++;
                    }
                }
                for (String s : cuentasOrden) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleCargo) o).getImporte()));
                        r.getCell(m++).setCellValue(((DetalleCargo) o).getImporte().doubleValue());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        totales.set(m - 3, totales.get(m - 3).add(((DetalleAbono) o).getImporte().negate())); 
                        r.getCell(m++).setCellValue(((DetalleAbono) o).getImporte().negate().doubleValue());
                    } else {
                        m++;
                    }
                }
            }

        }
        
        // cálculo de totales
        Row rTotales = sheet1.createRow(c);
        rTotales.createCell(2).setCellValue("TOTALES");
        rTotales.getCell(2).setCellStyle(style2);
        
        for (int n = 3; n < nMontos + 3; n++) {
            Cell cell = rTotales.createCell(n);
            cell.setCellStyle(style5);
            cell.setCellValue(totales.get(n - 3).doubleValue());
        }
        
        


        // escribimos el documento
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            wb.write(baos);
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());
            fileInputStream = bis;
        } catch (Exception e) {
            System.err.println(e + ": " + e.getMessage());
            throw e;
        }

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "downloadAsExcel";
    }

    public long getRuc() {
        return ruc;
    }

    public void setRuc(long ruc) {
        this.ruc = ruc;
    }

    public int getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(int idLibro) {
        this.idLibro = idLibro;
    }

    public EmpresaCliente getEmpresaCliente() {
        return empresaCliente;
    }

    public void setEmpresaCliente(EmpresaCliente empresaCliente) {
        this.empresaCliente = empresaCliente;
    }

    public LibroDiarioSimplificado getLds() {
        return lds;
    }

    public void setLds(LibroDiarioSimplificado lds) {
        this.lds = lds;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public String getFileName() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
        String periodo = sdf.format(this.getLds().getPeriodo());
        String rucPropietario = Long.toString(this.getRuc());
        return rucPropietario + " - Libro Diario de Formato Simplificado - " + periodo;
    }
}
