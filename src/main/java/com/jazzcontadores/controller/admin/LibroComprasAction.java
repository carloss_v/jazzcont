/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.controller.admin;

import com.jazzcontadores.model.dao.EmpresaClienteDAO;
import com.jazzcontadores.model.entities.DetalleLibroRegistroCompras;
import com.jazzcontadores.model.entities.EmpresaCliente;
import com.jazzcontadores.model.entities.LibroRegistroCompras;
import com.jazzcontadores.util.DAOFactory;
import com.jazzcontadores.util.HibernateUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Venegas
 */
public class LibroComprasAction extends ActionSupport {

    private long ruc;
    private int idLibro;
    private LibroRegistroCompras lrc;
    private int numeroCompras;
    private BigDecimal basesImponible1;
    private BigDecimal igvs1;
    private BigDecimal basesImponible2;
    private BigDecimal igvs2;
    private BigDecimal basesImponible3;
    private BigDecimal igvs3;
    private BigDecimal valorAdquisicionesNoGravadas;
    private BigDecimal isc;
    private BigDecimal otrosTributos;
    private BigDecimal importeTotal;
    private EmpresaCliente empresaCliente;
    private InputStream fileInputStream;

    public String show() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        EmpresaCliente e = empresaDAO.findByRuc(this.getRuc());

        if (e != null) {
            this.setEmpresaCliente(e);
        } else {
            return ERROR;
        }

        BigDecimal t_basesImponible1 = new BigDecimal("0.00");
        t_basesImponible1 = t_basesImponible1.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs1 = new BigDecimal("0.00");
        t_igvs1 = t_igvs1.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_basesImponible2 = new BigDecimal("0.00");
        t_basesImponible2 = t_basesImponible2.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs2 = new BigDecimal("0.00");
        t_igvs2 = t_igvs2.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_basesImponible3 = new BigDecimal("0.00");
        t_basesImponible3 = t_basesImponible3.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs3 = new BigDecimal("0.00");
        t_igvs3 = t_igvs3.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_valorAdquisicionesNoGravadas = new BigDecimal("0.00");
        t_valorAdquisicionesNoGravadas = t_valorAdquisicionesNoGravadas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_isc = new BigDecimal("0.00");
        t_isc = t_isc.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_otrosTributos = new BigDecimal("0.00");
        t_otrosTributos = t_otrosTributos.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotal = new BigDecimal("0.00");
        t_importeTotal = t_importeTotal.setScale(2, RoundingMode.HALF_EVEN);

        LibroRegistroCompras libro = factory.getLibroRegistroComprasDAO().findByIdAndEmpresa(this.getRuc(), this.getIdLibro());
        this.setLrc(libro);

        for (DetalleLibroRegistroCompras dlrc : libro.getDetallesLibroRegistroCompras()) {
            if (dlrc.getEsAdquisicionGravada()) {
                if (dlrc.getDestinoAdquisicionGravada().equals("gravada/exportacion")) {
                    t_basesImponible1 = t_basesImponible1.add(dlrc.getComprobanteCompra().getBase());
                    t_igvs1 = t_igvs1.add(dlrc.getComprobanteCompra().getIgv());
                } else if (dlrc.getDestinoAdquisicionGravada().equals("gravada/exportacion - no gravada")) {
                    t_basesImponible2 = t_basesImponible2.add(dlrc.getComprobanteCompra().getBase());
                    t_igvs2 = t_igvs2.add(dlrc.getComprobanteCompra().getIgv());
                } else if (dlrc.getDestinoAdquisicionGravada().equals("no gravada")) {
                    t_basesImponible3 = t_basesImponible3.add(dlrc.getComprobanteCompra().getBase());
                    t_igvs3 = t_igvs3.add(dlrc.getComprobanteCompra().getIgv());
                }
            } else {
                t_valorAdquisicionesNoGravadas = t_valorAdquisicionesNoGravadas.add(dlrc.getComprobanteCompra().getValorAdquisicionesNoGravadas());
            }

            if (dlrc.getComprobanteCompra().getIsc() != null) {
                t_isc = t_isc.add(dlrc.getComprobanteCompra().getIsc());
            }
            if (dlrc.getComprobanteCompra().getOtrosTributosYCargos() != null) {
                t_otrosTributos = t_otrosTributos.add(dlrc.getComprobanteCompra().getOtrosTributosYCargos());
            }
            if (dlrc.getComprobanteCompra().getImporteTotal() != null) {
                t_importeTotal = t_importeTotal.add(dlrc.getComprobanteCompra().getImporteTotal());
            }
        }

        // se settean los valores
        this.setNumeroCompras(libro.getDetallesLibroRegistroCompras().size());
        this.setBasesImponible1(t_basesImponible1);
        this.setIgvs1(t_igvs1);
        this.setBasesImponible2(t_basesImponible2);
        this.setIgvs2(t_igvs2);
        this.setBasesImponible3(t_basesImponible3);
        this.setIgvs3(t_igvs3);
        this.setValorAdquisicionesNoGravadas(t_valorAdquisicionesNoGravadas);
        this.setIsc(t_isc);
        this.setOtrosTributos(t_otrosTributos);
        this.setImporteTotal(t_importeTotal);

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "show";
    }

    public String downloadAsExcel() throws Exception {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        LibroRegistroCompras libro = factory.getLibroRegistroComprasDAO().findByIdAndEmpresa(this.getRuc(), this.getIdLibro());
        this.setLrc(libro);
        
        // Apache POI

        Workbook wb = new XSSFWorkbook();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
        Sheet sheet1 = wb.createSheet(sdf.format(this.getLrc().getPeriodo()));
        sheet1.setColumnWidth(0, 3072); // 19
        sheet1.setColumnWidth(1, 3584); //16
        sheet1.setColumnWidth(2, 3584);
        sheet1.setColumnWidth(3, 2560); // 13
        sheet1.setColumnWidth(4, 4608); // 18
        sheet1.setColumnWidth(5, 3584); // 14
        sheet1.setColumnWidth(6, 6656); // 26 n comprobante
        sheet1.setColumnWidth(7, 3328); // 13
        sheet1.setColumnWidth(8, 4608); // 18
        sheet1.setColumnWidth(9, 10240); // 40
        sheet1.setColumnWidth(10, 4352); // 17 montos
        sheet1.setColumnWidth(11, 4352);
        sheet1.setColumnWidth(12, 4352);
        sheet1.setColumnWidth(13, 4352);
        sheet1.setColumnWidth(14, 4352);
        sheet1.setColumnWidth(15, 4352);
        sheet1.setColumnWidth(16, 4352);
        sheet1.setColumnWidth(17, 4352);
        sheet1.setColumnWidth(18, 4352);
        sheet1.setColumnWidth(19, 4352);
        sheet1.setColumnWidth(20, 3584);
        sheet1.setColumnWidth(21, 3584); // numero const. dep.
        sheet1.setColumnWidth(22, 3584); // fecha emision const. dep.
        sheet1.setColumnWidth(23, 3072); // monto
        sheet1.setColumnWidth(24, 3584); // fecha comp. ref.
        sheet1.setColumnWidth(25, 2560); // tipo comp.
        sheet1.setColumnWidth(26, 4608); // serie
        sheet1.setColumnWidth(27, 6656); // n. comprobante

        // estilos para las celdas
        Font font = wb.createFont();
        font.setFontHeightInPoints((short) 15);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);

        Font font2 = wb.createFont(); // font2 = negrita solo              
        font2.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);

        CellStyle style1 = wb.createCellStyle();
        style1.setFont(font);
        
        CellStyle style2 = wb.createCellStyle();
        style2.setFont(font2);

        CellStyle style3 = wb.createCellStyle(); // estilo para los encabezados de la tabla
        style3.setFont(font2);
        style3.setWrapText(true);
        style3.setAlignment(CellStyle.ALIGN_CENTER);
        style3.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style3.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style3.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style3.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style3.setBorderLeft(XSSFCellStyle.BORDER_THIN);

        CellStyle style4 = wb.createCellStyle();
        style4.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style4.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style4.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style4.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        
        CellStyle style5 = wb.createCellStyle();
        style5.setFont(font2);
        style5.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style5.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style5.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style5.setBorderLeft(XSSFCellStyle.BORDER_THIN);

        // cabecera del libro
        Row row1 = sheet1.createRow((short) 0);
        Cell c0r1 = row1.createCell(0);
        c0r1.setCellValue("FORMATO 8.1: REGISTRO DE COMPRAS");
        c0r1.setCellStyle(style1);

        Row row2 = sheet1.createRow((short) 2);
        Cell c0r2 = row2.createCell(0);
        c0r2.setCellValue("PERIODO:");
        c0r2.setCellStyle(style2);

        Cell c1r2 = row2.createCell(1);
        c1r2.setCellValue(sdf.format(this.getLrc().getPeriodo()));

        Row row3 = sheet1.createRow((short) 3);
        Cell c0r3 = row3.createCell(0);
        c0r3.setCellValue("RUC:");
        c0r3.setCellStyle(style2);

        Cell c1r3 = row3.createCell(1);
        c1r3.setCellValue(Long.toString(this.getLrc().getEmpresaCliente().getRuc())); // para q no lo tome como long

        Row row4 = sheet1.createRow((short) 4);
        Cell c0r4 = row4.createCell(0);
        c0r4.setCellValue("APELLIDOS Y NOMBRES, DENOMINACIÓN O RAZÓN SOCIAL:");
        c0r4.setCellStyle(style2);

        row4.createCell(3).setCellValue(this.getLrc().getEmpresaCliente().getRazonSocial());

        // detalles del libro
        Row row5 = sheet1.createRow((short) 6);
        Row row6 = sheet1.createRow((short) 7);
        Row row7 = sheet1.createRow((short) 8);

        // creamos el encabezado de la tabla
        for (int i = 0; i < 28; i++) {
            Cell cell = row5.createCell(i);
            cell.setCellStyle(style3);
        }
        for (int i = 0; i < 28; i++) {
            Cell cell = row6.createCell(i);
            cell.setCellStyle(style3);
        }
        for (int i = 0; i < 28; i++) {
            Cell cell = row7.createCell(i);
            cell.setCellStyle(style3);
        }


        row5.getCell(0).setCellValue("NÚMERO CORRELATIVO DEL REGISTRO O CÓDIGO ÚNICO DE LA OPERACIÓN");
        row5.getCell(1).setCellValue("FECHA DE EMISIÓN DEL COMPROBANTE DE PAGO O DOCUMENTO");
        row5.getCell(2).setCellValue("FECHA DE VENCIMIENTO O FECHA DE PAGO");
        row5.getCell(3).setCellValue("COMPROBANTE DE PAGO O DOCUMENTO");
        row6.getCell(3).setCellValue("TIPO (TABLA 10)");
        row6.getCell(4).setCellValue("SERIE O CÓDIGO DE LA DEPENDENCIA ADUANERA (TABLA 11)");
        row6.getCell(5).setCellValue("AÑO DE EMISIÓN DE LA DUA O DSI");
        row5.getCell(6).setCellValue("N° DEL COMPROBANTE DE PAGO, DOCUMENTO, N° DE ORDEN DEL FORMULARIO FÍSICO O VIRTUAL, "
                + "N° DE DUA, DSI O LIQUIDACIÓN DE COBRANZA U OTROS DOCUMENTOS EMITIDOS POR SUNAT PARA ACREDITAR "
                + "EL CRÉDITO FISCAL EN LA IMPORTACIÓN");
        row5.getCell(7).setCellValue("INFORMACIÓN DEL PROVEEDOR");
        row6.getCell(7).setCellValue("DOCUMENTO DE IDENTIDAD");
        row7.getCell(7).setCellValue("TIPO (TABLA 2)");
        row7.getCell(8).setCellValue("NÚMERO");
        row6.getCell(9).setCellValue("APELLIDOS Y NOMBRES, DENOMINACIÓN O RAZÓN SOCIAL");
        row5.getCell(10).setCellValue("ADQUISICIONES GRAVADAS DESTINADAS A OPERACIONES GRAVADAS Y/O DE EXPORTACIÓN");
        row6.getCell(10).setCellValue("BASE IMPONIBLE");
        row6.getCell(11).setCellValue("IGV");
        row5.getCell(12).setCellValue("ADQUISICIONES GRAVADAS DESTINADAS A OPERACIONES GRAVADAS Y/O DE EXPORTACIÓN "
                + "Y A OPERACIONES NO GRAVADAS");
        row6.getCell(12).setCellValue("BASE IMPONIBLE");
        row6.getCell(13).setCellValue("IGV");
        row5.getCell(14).setCellValue("ADQUISICIONES GRAVADAS DESTINADAS A OPERACIONES NO GRAVADAS");
        row6.getCell(14).setCellValue("BASE IMPONIBLE");
        row6.getCell(15).setCellValue("IGV");
        row5.getCell(16).setCellValue("VALOR DE LAS ADQUISICIONES NO GRAVADAS");
        row5.getCell(17).setCellValue("ISC");
        row5.getCell(18).setCellValue("OTROS TRIBUTOS Y CARGOS");
        row5.getCell(19).setCellValue("IMPORTE TOTAL");
        row5.getCell(20).setCellValue("N° DE COMPROBANTE DE PAGO EMITIDO POR SUJETO NO DOMICILIADO");
        row5.getCell(21).setCellValue("CONSTANCIA DE DEPÓSITO DE DETRACCIÓN");
        row6.getCell(21).setCellValue("NÚMERO");
        row6.getCell(22).setCellValue("FECHA DE EMISIÓN");
        row5.getCell(23).setCellValue("TIPO DE CAMBIO");
        row5.getCell(24).setCellValue("REFERENCIA DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA");
        row6.getCell(24).setCellValue("FECHA");
        row6.getCell(25).setCellValue("TIPO (TABLA 10)");
        row6.getCell(26).setCellValue("SERIE");
        row6.getCell(27).setCellValue("N° DEL COMPROBANTE DE PAGO O DOCUMENTO");

        // merging cells
        sheet1.addMergedRegion(new CellRangeAddress(4, 4, 0, 2)); // razon social
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 0, 0)); // n correlativo
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 1, 1)); // fecha emision
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 2, 2)); // fecha pago
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 3, 5)); // comp. pago o doc
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 3, 3)); // tipo
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 4, 4)); // serie
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 5, 5)); // anio emision
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 6, 6)); // n. de comprobante
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 7, 9)); // info del prov.
        sheet1.addMergedRegion(new CellRangeAddress(7, 7, 7, 8)); // doc. identidad del prov.
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 9, 9)); // razon social de prov.
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 10, 11)); //
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 10, 10));
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 11, 11));
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 12, 13)); //
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 12, 12));
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 13, 13));
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 14, 15)); //
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 14, 14));
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 15, 15));
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 16, 16)); // valor adq. no gravadas
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 17, 17)); // isc
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 18, 18)); // otros tributos y cargos
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 19, 19)); // importe total
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 20, 20)); // n. comp. pago suj. no dom
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 21, 22)); // const. dep. detracc.
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 21, 21));
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 22, 22));
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 23, 23));
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 24, 27));
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 24, 24));
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 25, 25));
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 26, 26));
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 27, 27));

        int i = 9;
        BigDecimal t_basesImponible1 = new BigDecimal("0.00");
        t_basesImponible1 = t_basesImponible1.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs1 = new BigDecimal("0.00");
        t_igvs1 = t_igvs1.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_basesImponible2 = new BigDecimal("0.00");
        t_basesImponible2 = t_basesImponible2.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs2 = new BigDecimal("0.00");
        t_igvs2 = t_igvs2.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_basesImponible3 = new BigDecimal("0.00");
        t_basesImponible3 = t_basesImponible3.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs3 = new BigDecimal("0.00");
        t_igvs3 = t_igvs3.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_valorAdquisicionesNoGravadas = new BigDecimal("0.00");
        t_valorAdquisicionesNoGravadas = t_valorAdquisicionesNoGravadas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_isc = new BigDecimal("0.00");
        t_isc = t_isc.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_otrosTributos = new BigDecimal("0.00");
        t_otrosTributos = t_otrosTributos.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotal = new BigDecimal("0.00");
        t_importeTotal = t_importeTotal.setScale(2, RoundingMode.HALF_EVEN);
        
        // filas de la tabla
        for (DetalleLibroRegistroCompras d : this.getLrc().getDetallesLibroRegistroCompras()) {
            Row r = sheet1.createRow(i++);
            for (int j = 0; j < 28; j++) {
                Cell cell = r.createCell(j);
                cell.setCellStyle(style4);
            }
            
            // valores
            r.getCell(0).setCellValue(d.getNumeroCorrelativo());
            r.getCell(1).setCellValue(d.getComprobanteCompra().getFechaEmision());
            if (d.getComprobanteCompra().getFechaVencimientoOpago() != null) {
                r.getCell(2).setCellValue(d.getComprobanteCompra().getFechaVencimientoOpago());
            }
            r.getCell(3).setCellValue(d.getComprobanteCompra().getTipoComprobantePagoODocumento().getNumero());
            if (d.getComprobanteCompra().getCodigoAduana() != null) {
                r.getCell(4).setCellValue(d.getComprobanteCompra().getCodigoAduana().getNumero());
            } else {
                r.getCell(4).setCellValue(d.getComprobanteCompra().getSerie());
            }
            if (d.getComprobanteCompra().getAnioEmisionDuaOdsi() != null) {
                r.getCell(5).setCellValue(d.getComprobanteCompra().getAnioEmisionDuaOdsi());
            }
            r.getCell(6).setCellValue(d.getComprobanteCompra().getNumero());
            if (d.getComprobanteCompra().getProveedor() != null) {
                r.getCell(7).setCellValue(d.getComprobanteCompra().getProveedor().getTipoDocumentoIdentidad().getNumero());
                r.getCell(8).setCellValue(d.getComprobanteCompra().getProveedor().getNumeroDocumentoIdentidad());
                r.getCell(9).setCellValue(d.getComprobanteCompra().getProveedor().getRazonSocial());
            }
            if (d.getEsAdquisicionGravada()) {
                if (d.getDestinoAdquisicionGravada().equals("gravada/exportacion")) {
                    r.getCell(10).setCellValue(d.getComprobanteCompra().getBase().doubleValue());
                    r.getCell(11).setCellValue(d.getComprobanteCompra().getIgv().doubleValue());
                    t_basesImponible1 = t_basesImponible1.add(d.getComprobanteCompra().getBase());
                    t_igvs1 = t_igvs1.add(d.getComprobanteCompra().getIgv());
                } else if (d.getDestinoAdquisicionGravada().equals("gravada/exportacion - no gravada")) {
                    r.getCell(12).setCellValue(d.getComprobanteCompra().getBase().doubleValue());
                    r.getCell(13).setCellValue(d.getComprobanteCompra().getIgv().doubleValue());
                    t_basesImponible2 = t_basesImponible2.add(d.getComprobanteCompra().getBase());
                    t_igvs2 = t_igvs2.add(d.getComprobanteCompra().getIgv());
                } else if (d.getDestinoAdquisicionGravada().equals("no gravada")) {
                    r.getCell(14).setCellValue(d.getComprobanteCompra().getBase().doubleValue());
                    r.getCell(15).setCellValue(d.getComprobanteCompra().getIgv().doubleValue());
                    t_basesImponible3 = t_basesImponible3.add(d.getComprobanteCompra().getBase());
                    t_igvs3 = t_igvs3.add(d.getComprobanteCompra().getIgv());
                }
            } else {
                r.getCell(16).setCellValue(d.getComprobanteCompra().getValorAdquisicionesNoGravadas().doubleValue());
                t_valorAdquisicionesNoGravadas = t_valorAdquisicionesNoGravadas.add(d.getComprobanteCompra().getValorAdquisicionesNoGravadas());
            }
            if (d.getComprobanteCompra().getIsc() != null) {
                r.getCell(17).setCellValue(d.getComprobanteCompra().getIsc().doubleValue());
                t_isc = t_isc.add(d.getComprobanteCompra().getIsc());
            }
            if (d.getComprobanteCompra().getOtrosTributosYCargos() != null) {
                r.getCell(18).setCellValue(d.getComprobanteCompra().getOtrosTributosYCargos().doubleValue());
                t_otrosTributos = t_otrosTributos.add(d.getComprobanteCompra().getIsc());
            }
            r.getCell(19).setCellValue(d.getComprobanteCompra().getImporteTotal().doubleValue());
            t_importeTotal = t_importeTotal.add(d.getComprobanteCompra().getImporteTotal());
            if (d.getNumeroCompPagoSujNoDom() != null) {
                r.getCell(20).setCellValue(d.getNumeroCompPagoSujNoDom());
            }
            if (d.getNumeroConstDepDetraccion() != null) {
                r.getCell(21).setCellValue(d.getNumeroConstDepDetraccion());
            }
            if (d.getFechaEmisionConstDepDetraccion() != null) {
                r.getCell(22).setCellValue(d.getFechaEmisionConstDepDetraccion());
            }
            if (d.getTipoCambio() != null) {
                r.getCell(23).setCellValue(d.getTipoCambio().doubleValue());
            }
            if (d.getComprobanteCompraReferenciado() != null) {
                r.getCell(24).setCellValue(d.getComprobanteCompraReferenciado().getFechaEmision());
                r.getCell(25).setCellValue(d.getComprobanteCompraReferenciado().getTipoComprobantePagoODocumento().getNumero());
                r.getCell(26).setCellValue(d.getComprobanteCompraReferenciado().getSerie());
                r.getCell(27).setCellValue(d.getComprobanteCompraReferenciado().getNumero());
            }
        }
        
        // cálculo de totales
        Row rTotales = sheet1.createRow(i);
        rTotales.createCell(9).setCellValue("Totales");
        rTotales.getCell(9).setCellStyle(style2);
        
        for (int j = 10; j < 20; j++) {
            Cell cell = rTotales.createCell(j);
            cell.setCellStyle(style5);
        }
        
        rTotales.getCell(10).setCellValue(t_basesImponible1.doubleValue());
        rTotales.getCell(11).setCellValue(t_igvs1.doubleValue());
        rTotales.getCell(12).setCellValue(t_basesImponible2.doubleValue());
        rTotales.getCell(13).setCellValue(t_igvs2.doubleValue());
        rTotales.getCell(14).setCellValue(t_basesImponible3.doubleValue());
        rTotales.getCell(15).setCellValue(t_igvs3.doubleValue());
        rTotales.getCell(16).setCellValue(t_valorAdquisicionesNoGravadas.doubleValue());
        rTotales.getCell(17).setCellValue(t_isc.doubleValue());
        rTotales.getCell(18).setCellValue(t_otrosTributos.doubleValue());
        rTotales.getCell(19).setCellValue(t_importeTotal.doubleValue());        

        // escribimos el documento
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            wb.write(baos);
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());
            fileInputStream = bis;
        } catch (Exception e) {
            System.err.println(e + ": " + e.getMessage());
            throw e;
        }

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "downloadAsExcel";
    }

    public long getRuc() {
        return ruc;
    }

    public void setRuc(long ruc) {
        this.ruc = ruc;
    }

    public EmpresaCliente getEmpresaCliente() {
        return empresaCliente;
    }

    public void setEmpresaCliente(EmpresaCliente empresaCliente) {
        this.empresaCliente = empresaCliente;
    }

    public int getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(int idLibro) {
        this.idLibro = idLibro;
    }

    public BigDecimal getBasesImponible1() {
        return basesImponible1;
    }

    public void setBasesImponible1(BigDecimal basesImponible1) {
        this.basesImponible1 = basesImponible1;
    }

    public BigDecimal getIgvs1() {
        return igvs1;
    }

    public void setIgvs1(BigDecimal igvs1) {
        this.igvs1 = igvs1;
    }

    public BigDecimal getBasesImponible2() {
        return basesImponible2;
    }

    public void setBasesImponible2(BigDecimal basesImponible2) {
        this.basesImponible2 = basesImponible2;
    }

    public BigDecimal getIgvs2() {
        return igvs2;
    }

    public void setIgvs2(BigDecimal igvs2) {
        this.igvs2 = igvs2;
    }

    public BigDecimal getBasesImponible3() {
        return basesImponible3;
    }

    public void setBasesImponible3(BigDecimal basesImponible3) {
        this.basesImponible3 = basesImponible3;
    }

    public BigDecimal getIgvs3() {
        return igvs3;
    }

    public void setIgvs3(BigDecimal igvs3) {
        this.igvs3 = igvs3;
    }

    public BigDecimal getValorAdquisicionesNoGravadas() {
        return valorAdquisicionesNoGravadas;
    }

    public void setValorAdquisicionesNoGravadas(BigDecimal valorAdquisicionesNoGravadas) {
        this.valorAdquisicionesNoGravadas = valorAdquisicionesNoGravadas;
    }

    public BigDecimal getIsc() {
        return isc;
    }

    public void setIsc(BigDecimal isc) {
        this.isc = isc;
    }

    public BigDecimal getOtrosTributos() {
        return otrosTributos;
    }

    public void setOtrosTributos(BigDecimal otrosTributos) {
        this.otrosTributos = otrosTributos;
    }

    public BigDecimal getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }

    public int getNumeroCompras() {
        return numeroCompras;
    }

    public void setNumeroCompras(int numeroCompras) {
        this.numeroCompras = numeroCompras;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public LibroRegistroCompras getLrc() {
        return lrc;
    }

    public void setLrc(LibroRegistroCompras lrc) {
        this.lrc = lrc;
    }

    public String getFileName() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
        String periodo = sdf.format(this.getLrc().getPeriodo());
        String rucPropietario = Long.toString(this.getRuc());
        return rucPropietario + " - Registro de Compras - " + periodo;
    }
}
