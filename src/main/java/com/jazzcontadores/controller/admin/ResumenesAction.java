/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.controller.admin;

import com.jazzcontadores.extra.JCConstants;
import com.jazzcontadores.model.dao.EmpresaClienteDAO;
import com.jazzcontadores.model.entities.DetalleLibroRegistroCompras;
import com.jazzcontadores.model.entities.DetalleLibroRegistroVentas;
import com.jazzcontadores.model.entities.EmpresaCliente;
import com.jazzcontadores.model.entities.LibroRegistroCompras;
import com.jazzcontadores.model.entities.LibroRegistroVentas;
import com.jazzcontadores.util.DAOFactory;
import com.jazzcontadores.util.HibernateUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Venegas
 */
public class ResumenesAction extends ActionSupport {

    private long ruc;
    private String periodo;
    private byte rango; // tipo de estadisticas a mostrar
    private EmpresaCliente empresaCliente;
    private Map periodosLibros;
    // compras
    private LibroRegistroCompras lrc;
    private int numeroCompras;
    private BigDecimal basesImponible1;
    private BigDecimal igvs1;
    private BigDecimal basesImponible2;
    private BigDecimal igvs2;
    private BigDecimal basesImponible3;
    private BigDecimal igvs3;
    private BigDecimal valorAdquisicionesNoGravadas;
    private BigDecimal iscC;
    private BigDecimal otrosTributosC;
    private BigDecimal valorAdquisicionesGravadas;
    private BigDecimal importeTotalC;
    // ventas
    private LibroRegistroVentas lrv;
    private int numeroVentas;
    private BigDecimal valorFacturadoExportaciones;
    private BigDecimal baseImponibleOperacionesGravadas;
    private BigDecimal importeTotalOperacionesExoneradas;
    private BigDecimal importeTotalOperacionesInafectas;
    private BigDecimal iscV;
    private BigDecimal igvsV;
    private BigDecimal otrosTributosV;
    private BigDecimal valorAdquisicionesGravadasV;
    private BigDecimal valorAdquisicionesNoGravadasV;
    private BigDecimal importeTotalV;
    // tributo
    private BigDecimal tributo;
    private BigDecimal impuestoRenta;
    private BigDecimal impuestoIgv;

    public String list() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();

        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        EmpresaCliente e = empresaDAO.findByRuc(ruc);

        if (e != null) {
            // inicializamos el lazy load para mostrar en la vista
            e.getLibrosRegistroCompras().size();
            e.getLibrosRegistroVentas().size();
            e.getLibrosDiarioSimplificados().size();
            this.setEmpresaCliente(e);
        } else {
            return ERROR;
        }

        // combinar los libros compras con ventas

        Map<String, String> periodosLibrosUnsorted = new LinkedHashMap<String, String>(); // para mantener el orden de inserción

        for (LibroRegistroCompras l : e.getLibrosRegistroCompras()) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
            periodosLibrosUnsorted.put(sdf.format(l.getPeriodo()), sdf.format(l.getPeriodo()));
        }

        for (LibroRegistroCompras l1 : e.getLibrosRegistroCompras()) {
            boolean found = false;
            int i = -1;
            for (LibroRegistroVentas l2 : e.getLibrosRegistroVentas()) {
                i++;
                if (l1.getPeriodo().equals(l2.getPeriodo())) {
                    found = true;
                    break;
                }
            }
            if (found == false && i > -1) {
                SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
                periodosLibrosUnsorted.put(sdf.format(e.getLibrosRegistroVentas().get(i).getPeriodo()), sdf.format(e.getLibrosRegistroVentas().get(i).getPeriodo()));
            }
        }

        // ordenamos el mapa
        List list = new LinkedList(periodosLibrosUnsorted.entrySet());
        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) o1).getValue())
                        .compareTo(((Map.Entry) o2).getValue());
            }
        });

        // creamos el mapa ordenado
        Map periodosLibrosSorted = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            periodosLibrosSorted.put(entry.getKey(), entry.getValue());
        }
        this.setPeriodosLibros(periodosLibrosSorted);

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "list";
    }

    public String show() throws ParseException {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        EmpresaCliente e = empresaDAO.findByRuc(ruc);

        if (e != null) {
            this.setEmpresaCliente(e);
        } else {
            return ERROR;
        }

        // compras
        BigDecimal t_basesImponible1 = new BigDecimal("0.00");
        t_basesImponible1 = t_basesImponible1.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs1 = new BigDecimal("0.00");
        t_igvs1 = t_igvs1.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_basesImponible2 = new BigDecimal("0.00");
        t_basesImponible2 = t_basesImponible2.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs2 = new BigDecimal("0.00");
        t_igvs2 = t_igvs2.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_basesImponible3 = new BigDecimal("0.00");
        t_basesImponible3 = t_basesImponible3.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs3 = new BigDecimal("0.00");
        t_igvs3 = t_igvs3.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_valorAdquisicionesNoGravadas = new BigDecimal("0.00");
        t_valorAdquisicionesNoGravadas = t_valorAdquisicionesNoGravadas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_isc = new BigDecimal("0.00");
        t_isc = t_isc.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_otrosTributos = new BigDecimal("0.00");
        t_otrosTributos = t_otrosTributos.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_valorAdquisicionesGravadas = new BigDecimal("0.00");
        t_valorAdquisicionesGravadas = t_valorAdquisicionesGravadas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotal = new BigDecimal("0.00");
        t_importeTotal = t_importeTotal.setScale(2, RoundingMode.HALF_EVEN);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        LibroRegistroCompras libroCompras = factory.getLibroRegistroComprasDAO().findByPeriodo(this.getRuc(), sdf.parse("01-" + this.getPeriodo()));

        if (libroCompras != null) {
            this.setLrc(libroCompras);

            for (DetalleLibroRegistroCompras dlrc : libroCompras.getDetallesLibroRegistroCompras()) {
                if (dlrc.getEsAdquisicionGravada()) {
                    if (dlrc.getDestinoAdquisicionGravada().equals("gravada/exportacion")) {
                        t_basesImponible1 = t_basesImponible1.add(dlrc.getComprobanteCompra().getBase());
                        t_igvs1 = t_igvs1.add(dlrc.getComprobanteCompra().getIgv());
                    } else if (dlrc.getDestinoAdquisicionGravada().equals("gravada/exportacion - no gravada")) {
                        t_basesImponible2 = t_basesImponible2.add(dlrc.getComprobanteCompra().getBase());
                        t_igvs2 = t_igvs2.add(dlrc.getComprobanteCompra().getIgv());
                    } else if (dlrc.getDestinoAdquisicionGravada().equals("no gravada")) {
                        t_basesImponible3 = t_basesImponible3.add(dlrc.getComprobanteCompra().getBase());
                        t_igvs3 = t_igvs3.add(dlrc.getComprobanteCompra().getIgv());
                    }
                } else {
                    t_valorAdquisicionesNoGravadas = t_valorAdquisicionesNoGravadas.add(dlrc.getComprobanteCompra().getValorAdquisicionesNoGravadas());
                }

                if (dlrc.getComprobanteCompra().getIsc() != null) {
                    t_isc = t_isc.add(dlrc.getComprobanteCompra().getIsc());
                }
                if (dlrc.getComprobanteCompra().getOtrosTributosYCargos() != null) {
                    t_otrosTributos = t_otrosTributos.add(dlrc.getComprobanteCompra().getOtrosTributosYCargos());
                }
                t_valorAdquisicionesGravadas = t_basesImponible1.add(t_igvs1)
                        .add(t_basesImponible2).add(t_igvs2).add(t_basesImponible3).add(t_igvs3)
                        .add(t_isc).add(t_otrosTributos);

                t_importeTotal = t_importeTotal.add(dlrc.getComprobanteCompra().getImporteTotal());

            }
        }

        // se settean los valores
        this.setNumeroCompras((libroCompras != null) ? libroCompras.getDetallesLibroRegistroCompras().size() : 0);
        this.setBasesImponible1(t_basesImponible1);
        this.setIgvs1(t_igvs1);
        this.setBasesImponible2(t_basesImponible2);
        this.setIgvs2(t_igvs2);
        this.setBasesImponible3(t_basesImponible3);
        this.setIgvs3(t_igvs3);
        this.setValorAdquisicionesNoGravadas(t_valorAdquisicionesNoGravadas);
        this.setIscC(t_isc);
        this.setOtrosTributosC(t_otrosTributos);
        this.setValorAdquisicionesGravadas(t_valorAdquisicionesGravadas);
        this.setImporteTotalC(t_importeTotal);


        // ventas
        BigDecimal t_valorFacturadoExportaciones = new BigDecimal("0.00");
        t_valorFacturadoExportaciones = t_valorFacturadoExportaciones.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_baseImponibleOperacionesGravadas = new BigDecimal("0.00");
        t_baseImponibleOperacionesGravadas = t_baseImponibleOperacionesGravadas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotalOperacionesExoneradas = new BigDecimal("0.00");
        t_importeTotalOperacionesExoneradas = t_importeTotalOperacionesExoneradas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotalOperacionesInafectas = new BigDecimal("0.00");
        t_importeTotalOperacionesInafectas = t_importeTotalOperacionesInafectas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_iscV = new BigDecimal("0.00");
        t_iscV = t_iscV.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvsV = new BigDecimal("0.00");
        t_igvsV = t_igvsV.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_otrosTributosV = new BigDecimal("0.00");
        t_otrosTributosV = t_otrosTributosV.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_valorAdquisicionesGravadasV = new BigDecimal("0.00");
        t_valorAdquisicionesGravadasV = t_valorAdquisicionesGravadasV.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_valorAdquisicionesNoGravadasV = new BigDecimal("0.00");
        t_valorAdquisicionesNoGravadasV = t_valorAdquisicionesNoGravadasV.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotalV = new BigDecimal("0.00");
        t_importeTotalV = t_importeTotalV.setScale(2, RoundingMode.HALF_EVEN);

        LibroRegistroVentas libroVentas = factory.getLibroRegistroVentasDAO().findByPeriodo(this.getRuc(), sdf.parse("01-" + this.getPeriodo()));

        if (libroVentas != null) {
            this.setLrv(libroVentas);

            for (DetalleLibroRegistroVentas dlrv : libroVentas.getDetallesLibroRegistroVentas()) {
                if (dlrv.getComprobanteVenta().getValorFacturadoExportacion() != null) {
                    t_valorFacturadoExportaciones = t_valorFacturadoExportaciones
                            .add(dlrv.getComprobanteVenta().getValorFacturadoExportacion());
                }
                if (dlrv.getComprobanteVenta().getBaseImponibleOperacionGravada() != null) {
                    t_baseImponibleOperacionesGravadas = t_baseImponibleOperacionesGravadas
                            .add(dlrv.getComprobanteVenta().getBaseImponibleOperacionGravada());
                    t_valorAdquisicionesGravadasV.add(t_baseImponibleOperacionesGravadas);
                }
                if (dlrv.getComprobanteVenta().getTotalOperacionExonerada() != null) {
                    t_importeTotalOperacionesExoneradas = t_importeTotalOperacionesExoneradas
                            .add(dlrv.getComprobanteVenta().getTotalOperacionExonerada());
                    t_valorAdquisicionesNoGravadasV.add(t_importeTotalOperacionesExoneradas);
                }
                if (dlrv.getComprobanteVenta().getTotalOperacionInafecta() != null) {
                    t_importeTotalOperacionesInafectas = t_importeTotalOperacionesInafectas
                            .add(dlrv.getComprobanteVenta().getTotalOperacionInafecta());
                    t_valorAdquisicionesNoGravadasV.add(t_importeTotalOperacionesInafectas);
                }
                if (dlrv.getComprobanteVenta().getIsc() != null) {
                    t_iscV = t_iscV.add(dlrv.getComprobanteVenta().getIsc());
                    t_valorAdquisicionesGravadasV.add(t_iscV);
                }
                if (dlrv.getComprobanteVenta().getIgv() != null) {
                    t_igvsV = t_igvsV.add(dlrv.getComprobanteVenta().getIgv());
                    t_valorAdquisicionesGravadasV.add(t_igvsV);
                }
                if (dlrv.getComprobanteVenta().getOtrosTributos() != null) {
                    t_otrosTributosV = t_otrosTributosV.add(dlrv.getComprobanteVenta().getOtrosTributos());
                    t_valorAdquisicionesGravadasV.add(t_otrosTributosV);
                }
                if (dlrv.getComprobanteVenta().getImporteTotal() != null) {
                    t_importeTotalV = t_importeTotalV.add(dlrv.getComprobanteVenta().getImporteTotal());
                }
            } 
        }

        // se settean los valores
        this.setNumeroVentas((libroVentas != null) ? libroVentas.getDetallesLibroRegistroVentas().size() : 0);
        this.setValorFacturadoExportaciones(t_valorFacturadoExportaciones);
        this.setBaseImponibleOperacionesGravadas(t_baseImponibleOperacionesGravadas);
        this.setImporteTotalOperacionesExoneradas(t_importeTotalOperacionesExoneradas);
        this.setImporteTotalOperacionesInafectas(t_importeTotalOperacionesInafectas);
        this.setIscV(t_iscV);
        this.setIgvsV(t_igvsV);
        this.setOtrosTributosV(t_otrosTributosV);
        this.setValorAdquisicionesGravadasV(t_valorAdquisicionesGravadasV);
        this.setValorAdquisicionesNoGravadasV(t_valorAdquisicionesNoGravadasV);
        this.setImporteTotalV(t_importeTotalV);
        
        // cálculo del tributo        
        if (this.getEmpresaCliente().getTipoRegimen().equals(JCConstants.REGIMEN_RUS)) {
            String categoria = this.getEmpresaCliente().getCategoriaNRUS();
            if (categoria.equals("1")) {
                BigDecimal t_tributo = new BigDecimal("20.00");
                this.setTributo(t_tributo);                
            } else if (categoria.equals("2")) {
                BigDecimal t_tributo = new BigDecimal("50.00");
                this.setTributo(t_tributo); 
            } else if (categoria.equals("3")) {
                BigDecimal t_tributo = new BigDecimal("200.00");
                this.setTributo(t_tributo); 
            } else if (categoria.equals("4")) {
                BigDecimal t_tributo = new BigDecimal("400.00");
                this.setTributo(t_tributo); 
            } else if (categoria.equals("5")) {
                BigDecimal t_tributo = new BigDecimal("600.00");
                this.setTributo(t_tributo); 
            }
        } else if (this.getEmpresaCliente().getTipoRegimen().equals(JCConstants.REGIMEN_RER)) {
            BigDecimal t_impuestoRenta = t_importeTotalV.subtract(t_importeTotal).multiply(new BigDecimal("0.015"));
            this.setImpuestoRenta(t_impuestoRenta);   
            this.setImpuestoIgv(t_igvsV);
        } else if (this.getEmpresaCliente().getTipoRegimen().equals(JCConstants.REGIMEN_RG)) {
            // TO DO - Corregir fórmula
            BigDecimal t_tributo = t_importeTotalV.subtract(t_importeTotal).multiply(new BigDecimal("0.015"));
            this.setTributo(t_tributo);  
        }        

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "show";
    }

    public String showGraphics() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        EmpresaCliente e = empresaDAO.findByRuc(ruc);

        if (e != null) {
            this.setEmpresaCliente(e);
        } else {
            return ERROR;
        }

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "showGraphics";
    }

    public long getRuc() {
        return ruc;
    }

    public void setRuc(long ruc) {
        this.ruc = ruc;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public EmpresaCliente getEmpresaCliente() {
        return empresaCliente;
    }

    public void setEmpresaCliente(EmpresaCliente empresaCliente) {
        this.empresaCliente = empresaCliente;
    }

    public LibroRegistroCompras getLrc() {
        return lrc;
    }

    public void setLrc(LibroRegistroCompras lrc) {
        this.lrc = lrc;
    }

    public int getNumeroCompras() {
        return numeroCompras;
    }

    public void setNumeroCompras(int numeroCompras) {
        this.numeroCompras = numeroCompras;
    }

    public BigDecimal getBasesImponible1() {
        return basesImponible1;
    }

    public void setBasesImponible1(BigDecimal basesImponible1) {
        this.basesImponible1 = basesImponible1;
    }

    public BigDecimal getIgvs1() {
        return igvs1;
    }

    public void setIgvs1(BigDecimal igvs1) {
        this.igvs1 = igvs1;
    }

    public BigDecimal getBasesImponible2() {
        return basesImponible2;
    }

    public void setBasesImponible2(BigDecimal basesImponible2) {
        this.basesImponible2 = basesImponible2;
    }

    public BigDecimal getIgvs2() {
        return igvs2;
    }

    public void setIgvs2(BigDecimal igvs2) {
        this.igvs2 = igvs2;
    }

    public BigDecimal getBasesImponible3() {
        return basesImponible3;
    }

    public void setBasesImponible3(BigDecimal basesImponible3) {
        this.basesImponible3 = basesImponible3;
    }

    public BigDecimal getIgvs3() {
        return igvs3;
    }

    public void setIgvs3(BigDecimal igvs3) {
        this.igvs3 = igvs3;
    }

    public BigDecimal getValorAdquisicionesNoGravadas() {
        return valorAdquisicionesNoGravadas;
    }

    public void setValorAdquisicionesNoGravadas(BigDecimal valorAdquisicionesNoGravadas) {
        this.valorAdquisicionesNoGravadas = valorAdquisicionesNoGravadas;
    }

    public BigDecimal getIscC() {
        return iscC;
    }

    public void setIscC(BigDecimal iscC) {
        this.iscC = iscC;
    }

    public BigDecimal getOtrosTributosC() {
        return otrosTributosC;
    }

    public void setOtrosTributosC(BigDecimal otrosTributosC) {
        this.otrosTributosC = otrosTributosC;
    }

    public BigDecimal getImporteTotalC() {
        return importeTotalC;
    }

    public void setImporteTotalC(BigDecimal importeTotalC) {
        this.importeTotalC = importeTotalC;
    }

    public LibroRegistroVentas getLrv() {
        return lrv;
    }

    public void setLrv(LibroRegistroVentas lrv) {
        this.lrv = lrv;
    }

    public int getNumeroVentas() {
        return numeroVentas;
    }

    public void setNumeroVentas(int numeroVentas) {
        this.numeroVentas = numeroVentas;
    }

    public BigDecimal getValorFacturadoExportaciones() {
        return valorFacturadoExportaciones;
    }

    public void setValorFacturadoExportaciones(BigDecimal valorFacturadoExportaciones) {
        this.valorFacturadoExportaciones = valorFacturadoExportaciones;
    }

    public BigDecimal getBaseImponibleOperacionesGravadas() {
        return baseImponibleOperacionesGravadas;
    }

    public void setBaseImponibleOperacionesGravadas(BigDecimal baseImponibleOperacionesGravadas) {
        this.baseImponibleOperacionesGravadas = baseImponibleOperacionesGravadas;
    }

    public BigDecimal getImporteTotalOperacionesExoneradas() {
        return importeTotalOperacionesExoneradas;
    }

    public void setImporteTotalOperacionesExoneradas(BigDecimal importeTotalOperacionesExoneradas) {
        this.importeTotalOperacionesExoneradas = importeTotalOperacionesExoneradas;
    }

    public BigDecimal getImporteTotalOperacionesInafectas() {
        return importeTotalOperacionesInafectas;
    }

    public void setImporteTotalOperacionesInafectas(BigDecimal importeTotalOperacionesInafectas) {
        this.importeTotalOperacionesInafectas = importeTotalOperacionesInafectas;
    }

    public BigDecimal getIscV() {
        return iscV;
    }

    public void setIscV(BigDecimal iscV) {
        this.iscV = iscV;
    }

    public BigDecimal getIgvsV() {
        return igvsV;
    }

    public void setIgvsV(BigDecimal igvsV) {
        this.igvsV = igvsV;
    }

    public BigDecimal getOtrosTributosV() {
        return otrosTributosV;
    }

    public void setOtrosTributosV(BigDecimal otrosTributosV) {
        this.otrosTributosV = otrosTributosV;
    }

    public BigDecimal getImporteTotalV() {
        return importeTotalV;
    }

    public void setImporteTotalV(BigDecimal importeTotalV) {
        this.importeTotalV = importeTotalV;
    }

    public BigDecimal getValorAdquisicionesGravadas() {
        return valorAdquisicionesGravadas;
    }

    public void setValorAdquisicionesGravadas(BigDecimal valorAdquisicionesGravadas) {
        this.valorAdquisicionesGravadas = valorAdquisicionesGravadas;
    }

    public BigDecimal getValorAdquisicionesGravadasV() {
        return valorAdquisicionesGravadasV;
    }

    public void setValorAdquisicionesGravadasV(BigDecimal valorAdquisicionesGravadasV) {
        this.valorAdquisicionesGravadasV = valorAdquisicionesGravadasV;
    }

    public BigDecimal getValorAdquisicionesNoGravadasV() {
        return valorAdquisicionesNoGravadasV;
    }

    public void setValorAdquisicionesNoGravadasV(BigDecimal valorAdquisicionesNoGravadasV) {
        this.valorAdquisicionesNoGravadasV = valorAdquisicionesNoGravadasV;
    }

    public Map getPeriodosLibros() {
        return periodosLibros;
    }

    public void setPeriodosLibros(Map periodosLibros) {
        this.periodosLibros = periodosLibros;
    }

    public byte getRango() {
        return rango;
    }

    public void setRango(byte rango) {
        this.rango = rango;
    }

    public BigDecimal getTributo() {
        return tributo;
    }

    public void setTributo(BigDecimal tributo) {
        this.tributo = tributo;
    }

    public BigDecimal getImpuestoRenta() {
        return impuestoRenta;
    }

    public void setImpuestoRenta(BigDecimal impuestoRenta) {
        this.impuestoRenta = impuestoRenta;
    }

    public BigDecimal getImpuestoIgv() {
        return impuestoIgv;
    }

    public void setImpuestoIgv(BigDecimal impuestoIgv) {
        this.impuestoIgv = impuestoIgv;
    }
}
