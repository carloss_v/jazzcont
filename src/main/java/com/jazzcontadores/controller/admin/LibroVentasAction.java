/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.controller.admin;

import com.jazzcontadores.model.dao.EmpresaClienteDAO;
import com.jazzcontadores.model.entities.DetalleLibroRegistroVentas;
import com.jazzcontadores.model.entities.EmpresaCliente;
import com.jazzcontadores.model.entities.LibroRegistroVentas;
import com.jazzcontadores.util.DAOFactory;
import com.jazzcontadores.util.HibernateUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Venegas
 */
public class LibroVentasAction extends ActionSupport {

    private long ruc;
    private int idLibro;
    private LibroRegistroVentas lrv;
    private int numeroVentas;
    private BigDecimal valorFacturadoExportaciones;
    private BigDecimal baseImponibleOperacionesGravadas;
    private BigDecimal importeTotalOperacionesExoneradas;
    private BigDecimal importeTotalOperacionesInafectas;
    private BigDecimal isc;
    private BigDecimal igvs;
    private BigDecimal otrosTributos;
    private BigDecimal importeTotal;
    private EmpresaCliente empresaCliente;
    private InputStream fileInputStream;

    public String show() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        EmpresaCliente e = empresaDAO.findByRuc(this.getRuc());

        if (e != null) {
            this.setEmpresaCliente(e);
        } else {
            return ERROR;
        }
        
        BigDecimal t_valorFacturadoExportaciones = new BigDecimal("0.00");
        t_valorFacturadoExportaciones = t_valorFacturadoExportaciones.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_baseImponibleOperacionesGravadas = new BigDecimal("0.00");
        t_baseImponibleOperacionesGravadas = t_baseImponibleOperacionesGravadas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotalOperacionesExoneradas = new BigDecimal("0.00");
        t_importeTotalOperacionesExoneradas = t_importeTotalOperacionesExoneradas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotalOperacionesInafectas = new BigDecimal("0.00");
        t_importeTotalOperacionesInafectas = t_importeTotalOperacionesInafectas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_isc = new BigDecimal("0.00");
        t_isc = t_isc.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs = new BigDecimal("0.00");
        t_igvs = t_igvs.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_otrosTributos = new BigDecimal("0.00");
        t_otrosTributos = t_otrosTributos.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotal = new BigDecimal("0.00");
        t_importeTotal = t_importeTotal.setScale(2, RoundingMode.HALF_EVEN);
        
        LibroRegistroVentas libro = factory.getLibroRegistroVentasDAO().findByIdAndEmpresa(this.getRuc(), this.getIdLibro());
        this.setLrv(libro);
        
        for (DetalleLibroRegistroVentas dlrv : libro.getDetallesLibroRegistroVentas()) {
            if (dlrv.getComprobanteVenta().getValorFacturadoExportacion() != null) {
                t_valorFacturadoExportaciones = t_valorFacturadoExportaciones
                        .add(dlrv.getComprobanteVenta().getValorFacturadoExportacion());
            }
            if (dlrv.getComprobanteVenta().getBaseImponibleOperacionGravada() != null) {
                t_baseImponibleOperacionesGravadas = t_baseImponibleOperacionesGravadas
                        .add(dlrv.getComprobanteVenta().getBaseImponibleOperacionGravada());
            }
            if (dlrv.getComprobanteVenta().getTotalOperacionExonerada() != null) {
                t_importeTotalOperacionesExoneradas = t_importeTotalOperacionesExoneradas
                        .add(dlrv.getComprobanteVenta().getTotalOperacionExonerada());
            }
            if (dlrv.getComprobanteVenta().getTotalOperacionInafecta() != null) {
                t_importeTotalOperacionesInafectas = t_importeTotalOperacionesInafectas
                        .add(dlrv.getComprobanteVenta().getTotalOperacionInafecta());
            }
            if (dlrv.getComprobanteVenta().getIsc() != null) {
                t_isc = t_isc.add(dlrv.getComprobanteVenta().getIsc());
            }
            if (dlrv.getComprobanteVenta().getIgv() != null) {
                t_igvs = t_igvs.add(dlrv.getComprobanteVenta().getIgv());
            }
            if (dlrv.getComprobanteVenta().getOtrosTributos() != null) {
                t_otrosTributos = t_otrosTributos.add(dlrv.getComprobanteVenta().getOtrosTributos());
            }
            if (dlrv.getComprobanteVenta().getImporteTotal() != null) {
                t_importeTotal = t_importeTotal.add(dlrv.getComprobanteVenta().getImporteTotal());
            }
        }

        // se settean los valores
        this.setNumeroVentas(libro.getDetallesLibroRegistroVentas().size());
        this.setValorFacturadoExportaciones(t_valorFacturadoExportaciones);
        this.setBaseImponibleOperacionesGravadas(t_baseImponibleOperacionesGravadas);
        this.setImporteTotalOperacionesExoneradas(t_importeTotalOperacionesExoneradas);
        this.setImporteTotalOperacionesInafectas(t_importeTotalOperacionesInafectas);
        this.setIsc(t_isc);
        this.setIgvs(t_igvs);
        this.setOtrosTributos(t_otrosTributos);
        this.setImporteTotal(t_importeTotal);        
        
        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "show";
    }
    
    public String downloadAsExcel() throws Exception {
        
        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        
        LibroRegistroVentas libro = factory.getLibroRegistroVentasDAO().findByIdAndEmpresa(this.getRuc(), this.getIdLibro());
        this.setLrv(libro);
        
        // Apache POI
        
        Workbook wb = new XSSFWorkbook();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
        Sheet sheet1 = wb.createSheet(sdf.format(this.getLrv().getPeriodo()));
        sheet1.setColumnWidth(0, 3072); // 19
        sheet1.setColumnWidth(1, 3584); //16
        sheet1.setColumnWidth(2, 3584);
        sheet1.setColumnWidth(3, 2560); // 13
        sheet1.setColumnWidth(4, 4608); // 18
        sheet1.setColumnWidth(5, 6656); // 18 número de comprobante
        sheet1.setColumnWidth(6, 3328); // 26 tipo doc cliente
        sheet1.setColumnWidth(7, 4608); // 13 numero doc cliente
        sheet1.setColumnWidth(8, 10240); // 18 razon social cliente
        sheet1.setColumnWidth(9, 4352); // 17 valor fact. de la exportacion
        sheet1.setColumnWidth(10, 4352); // 17 base imponible de la op. gravada
        sheet1.setColumnWidth(11, 4352); // exonerada
        sheet1.setColumnWidth(12, 4352); // inafecta
        sheet1.setColumnWidth(13, 4352); // isc
        sheet1.setColumnWidth(14, 4352); // igv
        sheet1.setColumnWidth(15, 4352); // otros tributos
        sheet1.setColumnWidth(16, 4352); // importe total del comp. pago
        sheet1.setColumnWidth(17, 3072); // tipo de cambio
        sheet1.setColumnWidth(18, 3584); // fecha comp. ref.
        sheet1.setColumnWidth(19, 2560); // tipo comp. ref.
        sheet1.setColumnWidth(20, 4608); // serie comp. ref.
        sheet1.setColumnWidth(21, 6656); // n. comp. ref.
        
        // estilos para las celdas
        Font font = wb.createFont(); // font = negrita y tamaño 15
        font.setFontHeightInPoints((short) 15);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);

        Font font2 = wb.createFont(); // font2 = negrita solo              
        font2.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);

        CellStyle style1 = wb.createCellStyle();
        style1.setFont(font);
        
        CellStyle style2 = wb.createCellStyle();
        style2.setFont(font2);

        CellStyle style3 = wb.createCellStyle(); // estilo para los encabezados de la tabla
        style3.setFont(font2);
        style3.setWrapText(true);
        style3.setAlignment(CellStyle.ALIGN_CENTER);
        style3.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style3.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style3.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style3.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style3.setBorderLeft(XSSFCellStyle.BORDER_THIN);

        CellStyle style4 = wb.createCellStyle(); // estilo para las celdas de la tabla
        style4.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style4.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style4.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style4.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        
        CellStyle style5 = wb.createCellStyle(); // con border y negrita
        style5.setFont(font2);
        style5.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style5.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style5.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style5.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        
        // cabecera del libro
        Row row1 = sheet1.createRow((short) 0);
        Cell c0r1 = row1.createCell(0);
        c0r1.setCellValue("FORMATO 14.1: REGISTRO DE VENTAS E INGRESOS");
        c0r1.setCellStyle(style1);

        Row row2 = sheet1.createRow((short) 2);
        Cell c0r2 = row2.createCell(0);
        c0r2.setCellValue("PERIODO:");
        c0r2.setCellStyle(style2);

        Cell c1r2 = row2.createCell(1);
        c1r2.setCellValue(sdf.format(this.getLrv().getPeriodo()));

        Row row3 = sheet1.createRow((short) 3);
        Cell c0r3 = row3.createCell(0);
        c0r3.setCellValue("RUC:");
        c0r3.setCellStyle(style2);

        Cell c1r3 = row3.createCell(1);
        c1r3.setCellValue(Long.toString(this.getLrv().getEmpresaCliente().getRuc())); // para q no lo tome como long

        Row row4 = sheet1.createRow((short) 4);
        Cell c0r4 = row4.createCell(0);
        c0r4.setCellValue("APELLIDOS Y NOMBRES, DENOMINACIÓN O RAZÓN SOCIAL:");
        c0r4.setCellStyle(style2);

        row4.createCell(3).setCellValue(this.getLrv().getEmpresaCliente().getRazonSocial());
        
        // detalles del libro
        Row row5 = sheet1.createRow((short) 6);
        Row row6 = sheet1.createRow((short) 7);
        Row row7 = sheet1.createRow((short) 8);

        // creamos el encabezado de la tabla
        for (int i = 0; i < 22; i++) {
            Cell cell = row5.createCell(i);
            cell.setCellStyle(style3);
        }
        for (int i = 0; i < 22; i++) {
            Cell cell = row6.createCell(i);
            cell.setCellStyle(style3);
        }
        for (int i = 0; i < 22; i++) {
            Cell cell = row7.createCell(i);
            cell.setCellStyle(style3);
        }
        
        row5.getCell(0).setCellValue("NÚMERO CORRELATIVO DEL REGISTRO O CÓDIGO ÚNICO DE LA OPERACIÓN");
        row5.getCell(1).setCellValue("FECHA DE EMISIÓN DEL COMPROBANTE DE PAGO O DOCUMENTO");
        row5.getCell(2).setCellValue("FECHA DE VENCIMIENTO Y/O PAGO");
        row5.getCell(3).setCellValue("COMPROBANTE DE PAGO O DOCUMENTO");
        row6.getCell(3).setCellValue("TIPO (TABLA 10)");
        row6.getCell(4).setCellValue("N° SERIE O N° DE SERIE DE LA MÁQUINA REGISTRADORA");
        row6.getCell(5).setCellValue("NÚMERO");
        row5.getCell(6).setCellValue("INFORMACIÓN DEL CLIENTE");
        row6.getCell(6).setCellValue("DOCUMENTO DE IDENTIDAD");
        row7.getCell(6).setCellValue("TIPO (TABLA 2)");
        row7.getCell(7).setCellValue("NÚMERO");
        row6.getCell(8).setCellValue("APELLIDOS Y NOMBRES, DENOMINACIÓN O RAZÓN SOCIAL");
        row5.getCell(9).setCellValue("VALOR FACTURADO DE LA EXPORTACIÓN");
        row5.getCell(10).setCellValue("BASE IMPONIBLE DE LA OPERACIÓN GRAVADA");
        row5.getCell(11).setCellValue("IMPORTE TOTAL DE LA OPERACIÓN EXONERADA O INAFECTA");
        row6.getCell(11).setCellValue("EXONERADA");
        row6.getCell(12).setCellValue("INAFECTA");
        row5.getCell(13).setCellValue("ISC");
        row5.getCell(14).setCellValue("IGV Y/O IPM");
        row5.getCell(15).setCellValue("OTROS TRIBUTOS Y CARGOS QUE NO FORMAN PARTE DE LA BASE IMPONIBLE");
        row5.getCell(16).setCellValue("IMPORTE TOTAL DEL COMPROBANTE DE PAGO");
        row5.getCell(17).setCellValue("TIPO DE CAMBIO");
        row5.getCell(18).setCellValue("REFERENCIA DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA");
        row6.getCell(18).setCellValue("FECHA");
        row6.getCell(19).setCellValue("TIPO (TABLA 10)");
        row6.getCell(20).setCellValue("SERIE");
        row6.getCell(21).setCellValue("N°DEL COMPROBANTE DE PAGO O DOCUMENTO");
              
        // merging cells
        sheet1.addMergedRegion(new CellRangeAddress(4, 4, 0, 2)); // razon social
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 0, 0)); // n correlativo
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 1, 1)); // fecha emision
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 2, 2)); // fecha pago
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 3, 5)); // comp. pago o doc
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 3, 3)); // tipo
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 4, 4)); // serie
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 5, 5)); // número
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 6, 8)); // información del cliente
        sheet1.addMergedRegion(new CellRangeAddress(7, 7, 6, 7)); // doc. identidad
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 8, 8)); // razon social de cliente
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 9, 9)); // valor fact. export.
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 10, 10)); // base imponible op. grav.
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 11, 12)); // importe total op. exonerada o inaf.
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 11, 11)); // exonerada
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 12, 12)); // inafecta
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 13, 13)); // isc
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 14, 14)); // igv
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 15, 15)); // otros tributos y cargos
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 16, 16)); // importe total
        sheet1.addMergedRegion(new CellRangeAddress(6, 8, 17, 17)); // tipo de cambio
        sheet1.addMergedRegion(new CellRangeAddress(6, 6, 18, 21)); // referencia del comprobante de pago
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 18, 18)); // fecha
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 19, 19)); // tipo
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 20, 20)); // serie
        sheet1.addMergedRegion(new CellRangeAddress(7, 8, 21, 21)); // n. comp.
        
        int i = 9;
        BigDecimal t_valorFacturadoExportaciones = new BigDecimal("0.00");
        t_valorFacturadoExportaciones = t_valorFacturadoExportaciones.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_baseImponibleOperacionesGravadas = new BigDecimal("0.00");
        t_baseImponibleOperacionesGravadas = t_baseImponibleOperacionesGravadas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotalOperacionesExoneradas = new BigDecimal("0.00");
        t_importeTotalOperacionesExoneradas = t_importeTotalOperacionesExoneradas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotalOperacionesInafectas = new BigDecimal("0.00");
        t_importeTotalOperacionesInafectas = t_importeTotalOperacionesInafectas.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_isc = new BigDecimal("0.00");
        t_isc = t_isc.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_igvs = new BigDecimal("0.00");
        t_igvs = t_igvs.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_otrosTributos = new BigDecimal("0.00");
        t_otrosTributos = t_otrosTributos.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal t_importeTotal = new BigDecimal("0.00");
        t_importeTotal = t_importeTotal.setScale(2, RoundingMode.HALF_EVEN);
        
        //filas de la tabla
        for (DetalleLibroRegistroVentas d : this.getLrv().getDetallesLibroRegistroVentas()) {
            Row r = sheet1.createRow(i++);
            for (int j = 0; j < 22; j++) {
                Cell cell = r.createCell(j);
                cell.setCellStyle(style4);
            }
            
            // valores
            r.getCell(0).setCellValue(d.getNumeroCorrelativo());
            r.getCell(1).setCellValue(d.getComprobanteVenta().getFechaEmision());
            if (d.getComprobanteVenta().getFechaVencimiento() != null) {
                r.getCell(2).setCellValue(d.getComprobanteVenta().getFechaVencimiento());
            }
            r.getCell(3).setCellValue(d.getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero());
            r.getCell(4).setCellValue(d.getComprobanteVenta().getSerie());
            r.getCell(5).setCellValue(d.getComprobanteVenta().getNumero());
            if (d.getComprobanteVenta().getComprador() != null) {
                r.getCell(6).setCellValue(d.getComprobanteVenta().getComprador().getTipoDocumentoIdentidad().getNumero());
                r.getCell(7).setCellValue(d.getComprobanteVenta().getComprador().getNumeroDocumentoIdentidad());
                r.getCell(8).setCellValue(d.getComprobanteVenta().getComprador().getRazonSocialONombres());
            }
            if (d.getComprobanteVenta().getValorFacturadoExportacion() != null) {
                r.getCell(9).setCellValue(d.getComprobanteVenta().getValorFacturadoExportacion().doubleValue());
                t_valorFacturadoExportaciones = t_valorFacturadoExportaciones.add(d.getComprobanteVenta().getValorFacturadoExportacion());
            }
            if (d.getComprobanteVenta().getBaseImponibleOperacionGravada() != null) {
                r.getCell(10).setCellValue(d.getComprobanteVenta().getBaseImponibleOperacionGravada().doubleValue());
                t_baseImponibleOperacionesGravadas = t_baseImponibleOperacionesGravadas.add(d.getComprobanteVenta().getBaseImponibleOperacionGravada());
            }
            if (d.getComprobanteVenta().getTotalOperacionExonerada() != null) {
                r.getCell(11).setCellValue(d.getComprobanteVenta().getTotalOperacionExonerada().doubleValue());
                t_importeTotalOperacionesExoneradas = t_importeTotalOperacionesExoneradas.add(d.getComprobanteVenta().getTotalOperacionExonerada());
            }
            if (d.getComprobanteVenta().getTotalOperacionInafecta() != null) {
                r.getCell(12).setCellValue(d.getComprobanteVenta().getTotalOperacionInafecta().doubleValue());
                t_importeTotalOperacionesInafectas = t_importeTotalOperacionesInafectas.add(d.getComprobanteVenta().getTotalOperacionInafecta());
            }
            if (d.getComprobanteVenta().getIsc() != null) {
                r.getCell(13).setCellValue(d.getComprobanteVenta().getIsc().doubleValue());
                t_isc = t_isc.add(d.getComprobanteVenta().getIsc());
            }
            if (d.getComprobanteVenta().getIgv() != null) {
                r.getCell(14).setCellValue(d.getComprobanteVenta().getIgv().doubleValue());
                t_igvs = t_igvs.add(d.getComprobanteVenta().getIgv());
            }
            if (d.getComprobanteVenta().getOtrosTributos() != null) {
                r.getCell(15).setCellValue(d.getComprobanteVenta().getOtrosTributos().doubleValue());
                t_otrosTributos = t_otrosTributos.add(d.getComprobanteVenta().getOtrosTributos());
            }
            r.getCell(16).setCellValue(d.getComprobanteVenta().getImporteTotal().doubleValue());
            t_importeTotal = t_importeTotal.add(d.getComprobanteVenta().getImporteTotal());
            if (d.getTipoCambio() != null) {
                r.getCell(17).setCellValue(d.getTipoCambio().doubleValue());
            }
            if (d.getComprobanteVentaReferenciado() != null) {
                r.getCell(18).setCellValue(d.getComprobanteVentaReferenciado().getFechaEmision());
                r.getCell(19).setCellValue(d.getComprobanteVentaReferenciado().getTipoComprobantePagoODocumento().getNumero());
                r.getCell(20).setCellValue(d.getComprobanteVentaReferenciado().getSerie());
                r.getCell(21).setCellValue(d.getComprobanteVentaReferenciado().getNumero());
            }          
                        
        }
        
        // cálculo de totales
        Row rTotales = sheet1.createRow(i);
        rTotales.createCell(8).setCellValue("Totales");
        rTotales.getCell(8).setCellStyle(style2);
        
        for (int j = 9; j < 17; j++) {
            Cell cell = rTotales.createCell(j);
            cell.setCellStyle(style5);
        }
        
        rTotales.getCell(9).setCellValue(t_valorFacturadoExportaciones.doubleValue());  
        rTotales.getCell(10).setCellValue(t_baseImponibleOperacionesGravadas.doubleValue());
        rTotales.getCell(11).setCellValue(t_importeTotalOperacionesExoneradas.doubleValue());
        rTotales.getCell(12).setCellValue(t_importeTotalOperacionesInafectas.doubleValue());
        rTotales.getCell(13).setCellValue(t_isc.doubleValue());
        rTotales.getCell(14).setCellValue(t_igvs.doubleValue());
        rTotales.getCell(15).setCellValue(t_otrosTributos.doubleValue());
        rTotales.getCell(16).setCellValue(t_importeTotal.doubleValue());        
          
        
        // escribimos el documento
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            wb.write(baos);
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());
            fileInputStream = bis;
        } catch (Exception e) {
            System.err.println(e + ": " + e.getMessage());
            throw e;
        }
        
        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "downloadAsExcel";
    }

    public long getRuc() {
        return ruc;
    }

    public void setRuc(long ruc) {
        this.ruc = ruc;
    }

    public int getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(int idLibro) {
        this.idLibro = idLibro;
    }

    public EmpresaCliente getEmpresaCliente() {
        return empresaCliente;
    }

    public void setEmpresaCliente(EmpresaCliente empresaCliente) {
        this.empresaCliente = empresaCliente;
    }

    public int getNumeroVentas() {
        return numeroVentas;
    }

    public void setNumeroVentas(int numeroVentas) {
        this.numeroVentas = numeroVentas;
    }

    public BigDecimal getValorFacturadoExportaciones() {
        return valorFacturadoExportaciones;
    }

    public void setValorFacturadoExportaciones(BigDecimal valorFacturadoExportaciones) {
        this.valorFacturadoExportaciones = valorFacturadoExportaciones;
    }

    public BigDecimal getBaseImponibleOperacionesGravadas() {
        return baseImponibleOperacionesGravadas;
    }

    public void setBaseImponibleOperacionesGravadas(BigDecimal baseImponibleOperacionesGravadas) {
        this.baseImponibleOperacionesGravadas = baseImponibleOperacionesGravadas;
    }

    public BigDecimal getImporteTotalOperacionesExoneradas() {
        return importeTotalOperacionesExoneradas;
    }

    public void setImporteTotalOperacionesExoneradas(BigDecimal importeTotalOperacionesExoneradas) {
        this.importeTotalOperacionesExoneradas = importeTotalOperacionesExoneradas;
    }

    public BigDecimal getImporteTotalOperacionesInafectas() {
        return importeTotalOperacionesInafectas;
    }

    public void setImporteTotalOperacionesInafectas(BigDecimal importeTotalOperacionesInafectas) {
        this.importeTotalOperacionesInafectas = importeTotalOperacionesInafectas;
    }

    public BigDecimal getIsc() {
        return isc;
    }

    public void setIsc(BigDecimal isc) {
        this.isc = isc;
    }

    public BigDecimal getIgvs() {
        return igvs;
    }

    public void setIgvs(BigDecimal igvs) {
        this.igvs = igvs;
    }

    public BigDecimal getOtrosTributos() {
        return otrosTributos;
    }

    public void setOtrosTributos(BigDecimal otrosTributos) {
        this.otrosTributos = otrosTributos;
    }

    public BigDecimal getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }

    public LibroRegistroVentas getLrv() {
        return lrv;
    }

    public void setLrv(LibroRegistroVentas lrv) {
        this.lrv = lrv;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }
    
    public String getFileName() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
        String periodo = sdf.format(this.getLrv().getPeriodo());
        String rucPropietario = Long.toString(this.getRuc());
        return rucPropietario + " - Registro de Ventas e Ingresos - " + periodo;
    }
}
