/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.controller.cliente;

import com.jazzcontadores.model.entities.AsientoContable;
import com.jazzcontadores.model.entities.Contacto;
import com.jazzcontadores.model.entities.CuentaContable;
import com.jazzcontadores.model.entities.DetalleAbono;
import com.jazzcontadores.model.entities.DetalleCargo;
import com.jazzcontadores.model.entities.DetalleLibroRegistroCompras;
import com.jazzcontadores.model.entities.DetalleLibroRegistroVentas;
import com.jazzcontadores.model.entities.LibroDiarioSimplificado;
import com.jazzcontadores.model.entities.LibroRegistroCompras;
import com.jazzcontadores.model.entities.LibroRegistroVentas;
import com.jazzcontadores.model.entitiessrl.DetalleLibroRegistroComprasSerializable;
import com.jazzcontadores.model.entitiessrl.DetalleLibroRegistroVentasSerializable;
import com.jazzcontadores.util.DAOFactory;
import com.jazzcontadores.util.HibernateUtil;
import com.jazzcontadores.util.UserAware;
import com.opensymphony.xwork2.ActionSupport;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Venegas
 */
public class LibrosAjaxAction extends ActionSupport implements UserAware {

    Object user;
    private boolean success; // for ext    
    private int idLibro;
    private int page;
    private int start;
    private int limit;
    private int totalCount;
    private List<DetalleLibroRegistroVentasSerializable> listaDetallesRVSrl = new ArrayList<DetalleLibroRegistroVentasSerializable>();
    private List<DetalleLibroRegistroComprasSerializable> listaDetallesRCSrl = new ArrayList<DetalleLibroRegistroComprasSerializable>();
    private List<Map<String, Object>> lineasDiarioSimplificado = new ArrayList<Map<String, Object>>();
    private Map<String, Object> metaData = new HashMap<String, Object>();
    private List<Map<String, Object>> totalesPeriodosCompras = new ArrayList<Map<String, Object>>();
    private List<Map<String, Object>> totalesPeriodosVentas = new ArrayList<Map<String, Object>>();

    public String listDetallesRV() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        long ruc = ((Contacto) user).getEmpresaCliente().getRuc();

        this.setTotalCount(factory.getLibroRegistroVentasDAO().getTotalCountOfDetallesByLibro(ruc, this.getIdLibro()));
        List<DetalleLibroRegistroVentas> detallesRV = factory.getLibroRegistroVentasDAO()
                .findDetallesByIdLibroAndEmpresa(ruc, this.getIdLibro(), this.getStart(), this.getLimit());

        for (DetalleLibroRegistroVentas detalle : detallesRV) {
            DetalleLibroRegistroVentasSerializable detalleSrl = new DetalleLibroRegistroVentasSerializable();
            detalleSrl.setIdDetalleLibroRegistroVentas(detalle.getIdDetalleLibroRegistroVentas());
            detalleSrl.setNumeroCorrelativo(detalle.getNumeroCorrelativo());
            detalleSrl.setNumeroFinalTicketOcintaDelDia(detalle.getNumeroFinalTicketOcintaDelDia());
            detalleSrl.setTipoCambio(detalle.getTipoCambio());
            detalleSrl.setEstadoOportunidadDeAnotación(detalle.getEstadoOportunidadDeAnotacion());
            detalleSrl.setFechaHoraRegistro(detalle.getFechaHoraRegistro());
            // comprobante
            detalleSrl.setIdComprobanteVenta(detalle.getComprobanteVenta().getIdComprobanteVenta());
            detalleSrl.setFechaEmisionComprobante(detalle.getComprobanteVenta().getFechaEmision());
            detalleSrl.setFechaVencimientoComprobante(detalle.getComprobanteVenta().getFechaVencimiento());
            detalleSrl.setValorFacturadoExportacion(detalle.getComprobanteVenta().getValorFacturadoExportacion());
            detalleSrl.setBaseImponibleOpGravada(detalle.getComprobanteVenta().getBaseImponibleOperacionGravada());
            detalleSrl.setTotalOperacionExonerada(detalle.getComprobanteVenta().getTotalOperacionExonerada());
            detalleSrl.setTotalOperacionInafecta(detalle.getComprobanteVenta().getTotalOperacionInafecta());
            detalleSrl.setIsc(detalle.getComprobanteVenta().getIsc());
            detalleSrl.setIgv(detalle.getComprobanteVenta().getIgv());
            detalleSrl.setBaseImponibleArrozPilado(detalle.getComprobanteVenta().getBaseImponibleArrozPilado());
            detalleSrl.setImpuestoVentasArrozPilado(detalle.getComprobanteVenta().getImpuestoVentasArrozPilado());
            detalleSrl.setOtrosTributos(detalle.getComprobanteVenta().getOtrosTributos());
            detalleSrl.setImporteTotalComprobante(detalle.getComprobanteVenta().getImporteTotal());
            detalleSrl.setNumeroTipoComprobante(detalle.getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero());
            detalleSrl.setSerieComprobante(detalle.getComprobanteVenta().getSerie());
            detalleSrl.setNumeroComprobante(detalle.getComprobanteVenta().getNumero());
            // comprador
            if (detalle.getComprobanteVenta().getComprador() != null) {
                detalleSrl.setNumeroTipoDocIdentidadComprador(detalle.getComprobanteVenta().getComprador().getTipoDocumentoIdentidad().getNumero());
                detalleSrl.setNumeroDocIdentidadComprador(detalle.getComprobanteVenta().getComprador().getNumeroDocumentoIdentidad());
                detalleSrl.setRazonSocialONombresComprador(detalle.getComprobanteVenta().getComprador().getRazonSocialONombres());
            }
            // comprobante referenciado
            if (detalle.getComprobanteVentaReferenciado() != null) {
                detalleSrl.setIdComprobanteVentaReferenciado(detalle.getComprobanteVentaReferenciado().getIdComprobanteVenta());
                detalleSrl.setFechaComprobanteVentaReferenciado(detalle.getComprobanteVentaReferenciado().getFechaEmision());
                detalleSrl.setNumeroTipoComprobanteVentaReferenciado(detalle.getComprobanteVentaReferenciado().getTipoComprobantePagoODocumento().getNumero());
                detalleSrl.setSerieComprobanteVentaReferenciado(detalle.getComprobanteVentaReferenciado().getSerie());
                detalleSrl.setNumeroComprobanteVentaReferenciado(detalle.getComprobanteVentaReferenciado().getNumero());
            }

            this.getListaDetallesRVSrl().add(detalleSrl);
        }

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        this.setSuccess(true);
        return "listDetallesRV";
    }

    public String listDetallesRC() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        long ruc = ((Contacto) user).getEmpresaCliente().getRuc();

        this.setTotalCount(factory.getLibroRegistroComprasDAO().getTotalCountOfDetallesByLibro(ruc, this.getIdLibro()));
        List<DetalleLibroRegistroCompras> detallesRC = factory.getLibroRegistroComprasDAO()
                .findDetallesByIdLibroAndEmpresa(ruc, this.getIdLibro(), this.getStart(), this.getLimit());

        for (DetalleLibroRegistroCompras detalle : detallesRC) {
            DetalleLibroRegistroComprasSerializable detalleSrl = new DetalleLibroRegistroComprasSerializable();
            detalleSrl.setIdDetalleLibroRegistroCompras(detalle.getIdDetalleLibroRegistroCompras());
            detalleSrl.setNumeroCorrelativo(detalle.getNumeroCorrelativo());
            detalleSrl.setDestinoAdquisicionGravada(detalle.getDestinoAdquisicionGravada());
            detalleSrl.setTipoCambio(detalle.getTipoCambio());
            detalleSrl.setNumeroCompPagoSujNoDom(detalle.getNumeroCompPagoSujNoDom());
            detalleSrl.setNumeroConstDepDetraccion(detalle.getNumeroConstDepDetraccion());
            detalleSrl.setFechaEmisionConstDepDetraccion(detalle.getFechaEmisionConstDepDetraccion());
            detalleSrl.setNumeroFinalOperDiariasSinCredFiscal(detalle.getNumeroFinalOperDiariasSinCredFiscal());
            detalleSrl.setMarcaComprobanteSujetoAretencion(detalle.getMarcaComprobanteSujetoAretencion());
            detalleSrl.setFechaHoraRegistro(detalle.getFechaHoraRegistro());
            if (detalle.getEsAdquisicionGravada()) {
                if (detalle.getDestinoAdquisicionGravada().equals("gravada/exportacion")) {
                    detalleSrl.setBaseImponible1(detalle.getComprobanteCompra().getBase());
                    detalleSrl.setIgv1(detalle.getComprobanteCompra().getIgv());
                } else if (detalle.getDestinoAdquisicionGravada().equals("gravada/exportacion - no gravada")) {
                    detalleSrl.setBaseImponible2(detalle.getComprobanteCompra().getBase());
                    detalleSrl.setIgv2(detalle.getComprobanteCompra().getIgv());
                } else if (detalle.getDestinoAdquisicionGravada().equals("no gravada")) {
                    detalleSrl.setBaseImponible3(detalle.getComprobanteCompra().getBase());
                    detalleSrl.setIgv3(detalle.getComprobanteCompra().getIgv());
                }
            }

            // comprobante
            detalleSrl.setIdComprobanteCompra(detalle.getComprobanteCompra().getIdComprobanteCompra());
            detalleSrl.setNumeroComprobante(detalle.getComprobanteCompra().getNumero());
            detalleSrl.setSerieComprobante(detalle.getComprobanteCompra().getSerie());
            detalleSrl.setAnioEmisionDuaOdsiComprobante(detalle.getComprobanteCompra().getAnioEmisionDuaOdsi());
            detalleSrl.setFechaEmisionComprobante(detalle.getComprobanteCompra().getFechaEmision());
            detalleSrl.setFechaVencimientoOpagoComprobante(detalle.getComprobanteCompra().getFechaVencimientoOpago());
            detalleSrl.setValorAdquisicionesNoGravadas(detalle.getComprobanteCompra().getValorAdquisicionesNoGravadas());
            detalleSrl.setIsc(detalle.getComprobanteCompra().getIsc());
            detalleSrl.setOtrosTributosYcargos(detalle.getComprobanteCompra().getOtrosTributosYCargos());
            detalleSrl.setImporteTotal(detalle.getComprobanteCompra().getImporteTotal());
            detalleSrl.setBaseComprobante(detalle.getComprobanteCompra().getBase());
            detalleSrl.setIgvComprobante(detalle.getComprobanteCompra().getIgv());
            detalleSrl.setImporteTotalComprobante(detalle.getComprobanteCompra().getImporteTotal());
            detalleSrl.setNumeroTipoComprobante(detalle.getComprobanteCompra().getTipoComprobantePagoODocumento().getNumero());
            if (detalle.getComprobanteCompra().getProveedor() != null) {
                detalleSrl.setNumeroTipoDocIdentidadProveedor(detalle.getComprobanteCompra().getProveedor().getTipoDocumentoIdentidad().getNumero());
                detalleSrl.setNumeroDocIdentidadProveedor(detalle.getComprobanteCompra().getProveedor().getNumeroDocumentoIdentidad());
                detalleSrl.setRazonSocialProveedor(detalle.getComprobanteCompra().getProveedor().getRazonSocial());
            }
            if (detalle.getComprobanteCompraReferenciado() != null) {
                detalleSrl.setIdComprobanteCompraReferenciado(detalle.getComprobanteCompraReferenciado().getIdComprobanteCompra());
                detalleSrl.setFechaComprobanteCompraReferenciado(detalle.getComprobanteCompraReferenciado().getFechaEmision());
                detalleSrl.setNumeroTipoComprobanteCompraReferenciado(detalle.getComprobanteCompraReferenciado().getTipoComprobantePagoODocumento().getNumero());
                detalleSrl.setSerieComprobanteCompraReferenciado(detalle.getComprobanteCompraReferenciado().getSerie());
                detalleSrl.setNumeroComprobanteCompraReferenciado(detalle.getComprobanteCompraReferenciado().getNumero());
            }

            this.getListaDetallesRCSrl().add(detalleSrl);
        }

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        this.setSuccess(true);
        return "listDetallesRC";
    }

    public String listAsientosContables() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        long ruc = ((Contacto) user).getEmpresaCliente().getRuc();
        LibroDiarioSimplificado lds = factory.getLibroDiarioSimplificadoDAO().findByIdAndEmpresa(ruc, this.getIdLibro());

        this.setTotalCount(lds.getAsientosContables().size());
        // obtener todas las cuentas implicadas en los asientos del libro
        List<CuentaContable> cuentas = new ArrayList<CuentaContable>();
        for (AsientoContable a : lds.getAsientosContables()) {
            for (DetalleCargo dc : a.getDetallesCargo()) {
                if (!cuentas.contains(dc.getRegistroCuentaContable().getCuentaContable())) {
                    cuentas.add(dc.getRegistroCuentaContable().getCuentaContable());
                }
            }
            for (DetalleAbono da : a.getDetallesAbono()) {
                if (!cuentas.contains(da.getRegistroCuentaContable().getCuentaContable())) {
                    cuentas.add(da.getRegistroCuentaContable().getCuentaContable());
                }
            }
        }
        // ordenamos la lista ascendentemente por el numero de cuenta
        Collections.sort(cuentas, new Comparator<CuentaContable>() {
            @Override
            public int compare(CuentaContable c1, CuentaContable c2) {
                return c1.getNumero().compareTo(c2.getNumero());
            }
        });

        for (CuentaContable c : cuentas) {
            System.out.println("numCuenta:" + c.getNumero());
        }


        // arreglos de tipos de cuentas
        List<String> activos = new ArrayList<String>();
        List<String> pasivos = new ArrayList<String>();
        List<String> patrimonio = new ArrayList<String>();
        List<String> gastos = new ArrayList<String>();
        List<String> ingresos = new ArrayList<String>();
        List<String> saldosIntermediarios = new ArrayList<String>();
        List<String> cuentasFuncionGasto = new ArrayList<String>();
        List<String> cuentasOrden = new ArrayList<String>();

        for (CuentaContable c : cuentas) {
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 0) {
                cuentasOrden.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 1
                    || c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 2
                    || c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 3) {
                activos.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 4) {
                pasivos.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 5) {
                patrimonio.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 6) {
                gastos.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 7) {
                ingresos.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 8) {
                saldosIntermediarios.add(c.getNumero());
            }
            if (c.getClasificacionCuenta().getElementoPlanCuentas().getIdElementoPlanCuentas() == 9) {
                cuentasFuncionGasto.add(c.getNumero());
            }
        }


        for (String s : activos) {
            System.out.println("act:" + s);
        }

        // creamos la lista de registros de libro
        List<Map<String, Object>> listaLineas = new ArrayList<Map<String, Object>>();
        int nMontos = activos.size() + pasivos.size() + patrimonio.size() + gastos.size()
                + ingresos.size() + saldosIntermediarios.size() + cuentasFuncionGasto.size() + cuentasOrden.size();

        for (AsientoContable a : lds.getAsientosContables()) {

            List detalles = new ArrayList();
            detalles.addAll(a.getDetallesAbono());
            detalles.addAll(a.getDetallesCargo());

            Map<String, Object> linea = new HashMap<String, Object>();
            linea.put("numeroCorrelativo", a.getNumeroCorrelativo());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            linea.put("fecha", sdf.format(a.getFecha()));
            linea.put("glosa", a.getGlosa());

            for (int n = 0; n < nMontos; n++) {
                linea.put("amount" + (n + 1), null);
            }

            for (Object o : detalles) { // los detalles tienen por lo menos 2 elementos (1 en el cargo y 1 en el abono)
                int j = 1;
                for (String s : activos) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        linea.put("amount" + j++, ((DetalleCargo) o).getImporte());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        linea.put("amount" + j++, ((DetalleAbono) o).getImporte().negate());
                    } else {
                        j++;
                    }
                }

                for (String s : pasivos) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        linea.put("amount" + j++, ((DetalleCargo) o).getImporte());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        linea.put("amount" + j++, ((DetalleAbono) o).getImporte().negate());
                    } else {
                        j++;
                    }
                }

                for (String s : patrimonio) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        linea.put("amount" + j++, ((DetalleCargo) o).getImporte());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        linea.put("amount" + j++, ((DetalleAbono) o).getImporte().negate());
                    } else {
                        j++;
                    }
                }

                for (String s : gastos) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        linea.put("amount" + j++, ((DetalleCargo) o).getImporte());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        linea.put("amount" + j++, ((DetalleAbono) o).getImporte().negate());
                    } else {
                        j++;
                    }
                }

                for (String s : ingresos) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        linea.put("amount" + j++, ((DetalleCargo) o).getImporte());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        linea.put("amount" + j++, ((DetalleAbono) o).getImporte().negate());
                    } else {
                        j++;
                    }
                }

                for (String s : saldosIntermediarios) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        linea.put("amount" + j++, ((DetalleCargo) o).getImporte());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        linea.put("amount" + j++, ((DetalleAbono) o).getImporte().negate());
                    } else {
                        j++;
                    }
                }

                for (String s : cuentasFuncionGasto) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        linea.put("amount" + j++, ((DetalleCargo) o).getImporte());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        linea.put("amount" + j++, ((DetalleAbono) o).getImporte().negate());
                    } else {
                        j++;
                    }
                }

                for (String s : cuentasOrden) {
                    if (o instanceof DetalleCargo && ((DetalleCargo) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        linea.put("amount" + j++, ((DetalleCargo) o).getImporte());
                    } else if (o instanceof DetalleAbono && ((DetalleAbono) o).getRegistroCuentaContable().getCuentaContable().getNumero().equals(s)) {
                        // cuando de abona una cuenta de activo disminuye el haber
                        linea.put("amount" + j++, ((DetalleAbono) o).getImporte().negate());
                    } else {
                        j++;
                    }
                }
            }

            listaLineas.add(linea);
        }

        // creamos la metaData
        List<Map<String, String>> fields = new ArrayList<Map<String, String>>();
        List<Map<String, Object>> columns = new ArrayList<Map<String, Object>>();

        Map<String, String> field = new HashMap<String, String>();
        field.put("name", "numeroCorrelativo");
        field.put("type", "auto");
        fields.add(field);
        field = new HashMap<String, String>();
        field.put("name", "fecha");
        field.put("type", "auto"); // con date no muestra bien el formato
        //field.put("dateFormat", "d/m/Y");
        fields.add(field);
        field = new HashMap<String, String>();
        field.put("name", "glosa");
        field.put("type", "auto");
        fields.add(field);

        for (int n = 0; n < nMontos; n++) {
            field = new HashMap<String, String>();
            field.put("name", "amount" + (n + 1));
            field.put("type", "auto");
            fields.add(field);
        }

        Map<String, Object> column = new HashMap<String, Object>();
        column.put("text", "Número correlativo o Código Único de la Operación");
        column.put("dataIndex", "numeroCorrelativo");
        column.put("width", 85);
        column.put("sortable", true);
        columns.add(column);
        column = new HashMap<String, Object>();
        column.put("text", "Fecha o período de la Operación");
        column.put("dataIndex", "fecha");
        column.put("width", 105);
        //column.put("renderer", "Ext.util.Format.dateRenderer('d/m/Y')");
        column.put("sortable", true);
        columns.add(column);
        column = new HashMap<String, Object>();
        column.put("text", "Glosa o descripción de la Operación");
        column.put("dataIndex", "glosa");
        column.put("width", 295);
        column.put("sortable", false);
        columns.add(column);

        int i = 1;
        // verificamos que haya cuenta de activos, de lo contrario no se agrega dicha columna
        // activos
        if (!activos.isEmpty()) {
            List<Map<String, Object>> subColumnas = new ArrayList<Map<String, Object>>();
            column = new HashMap<String, Object>();
            column.put("text", "ACTIVOS");
            for (String s : activos) {
                Map<String, Object> subcolumna = new HashMap<String, Object>();
                subcolumna.put("text", s);
                subcolumna.put("dataIndex", "amount" + i++);
                subcolumna.put("width", 105);
                subcolumna.put("sortable", false);
                subcolumna.put("tdCls", "align-right-td");
                subcolumna.put("summaryType", "sum");
                subColumnas.add(subcolumna);
            }
            column.put("columns", subColumnas);
            columns.add(column);
        }

        // pasivos
        if (!pasivos.isEmpty()) {
            List<Map<String, Object>> subColumnas = new ArrayList<Map<String, Object>>();
            column = new HashMap<String, Object>();
            column.put("text", "PASIVOS");
            for (String s : pasivos) {
                Map<String, Object> subcolumna = new HashMap<String, Object>();
                subcolumna.put("text", s);
                subcolumna.put("dataIndex", "amount" + i++);
                subcolumna.put("width", 105);
                subcolumna.put("sortable", false);
                subcolumna.put("tdCls", "align-right-td");
                subcolumna.put("summaryType", "sum");
                subColumnas.add(subcolumna);
            }
            column.put("columns", subColumnas);
            columns.add(column);
        }

        // patrimonio
        if (!patrimonio.isEmpty()) {
            List<Map<String, Object>> subColumnas = new ArrayList<Map<String, Object>>();
            column = new HashMap<String, Object>();
            column.put("text", "PATRIMONIO");
            for (String s : patrimonio) {
                Map<String, Object> subcolumna = new HashMap<String, Object>();
                subcolumna.put("text", s);
                subcolumna.put("dataIndex", "amount" + i++);
                subcolumna.put("width", 105);
                subcolumna.put("sortable", false);
                subcolumna.put("tdCls", "align-right-td");
                subcolumna.put("summaryType", "sum");
                subColumnas.add(subcolumna);
            }
            column.put("columns", subColumnas);
            columns.add(column);
        }

        // gastos
        if (!gastos.isEmpty()) {
            List<Map<String, Object>> subColumnas = new ArrayList<Map<String, Object>>();
            column = new HashMap<String, Object>();
            column.put("text", "GASTOS");
            for (String s : gastos) {
                Map<String, Object> subcolumna = new HashMap<String, Object>();
                subcolumna.put("text", s);
                subcolumna.put("dataIndex", "amount" + i++);
                subcolumna.put("width", 105);
                subcolumna.put("sortable", false);
                subcolumna.put("tdCls", "align-right-td");
                subcolumna.put("summaryType", "sum");
                subColumnas.add(subcolumna);
            }
            column.put("columns", subColumnas);
            columns.add(column);
        }

        // ingresos
        if (!ingresos.isEmpty()) {
            List<Map<String, Object>> subColumnas = new ArrayList<Map<String, Object>>();
            column = new HashMap<String, Object>();
            column.put("text", "INGRESOS");
            for (String s : ingresos) {
                Map<String, Object> subcolumna = new HashMap<String, Object>();
                subcolumna.put("text", s);
                subcolumna.put("dataIndex", "amount" + i++);
                subcolumna.put("width", 105);
                subcolumna.put("sortable", false);
                subcolumna.put("tdCls", "align-right-td");
                subcolumna.put("summaryType", "sum");
                subColumnas.add(subcolumna);
            }
            column.put("columns", subColumnas);
            columns.add(column);
        }

        // saldos intermediarios
        if (!saldosIntermediarios.isEmpty()) {
            List<Map<String, Object>> subColumnas = new ArrayList<Map<String, Object>>();
            column = new HashMap<String, Object>();
            column.put("text", "SALDOS INTERMEDIARIOS DE GESTIÓN");
            for (String s : saldosIntermediarios) {
                Map<String, Object> subcolumna = new HashMap<String, Object>();
                subcolumna.put("text", s);
                subcolumna.put("dataIndex", "amount" + i++);
                subcolumna.put("width", 105);
                subcolumna.put("sortable", false);
                subcolumna.put("tdCls", "align-right-td");
                subcolumna.put("summaryType", "sum");
                subColumnas.add(subcolumna);
            }
            column.put("columns", subColumnas);
            columns.add(column);
        }

        // cuentas del función del gasto
        if (!cuentasFuncionGasto.isEmpty()) {
            List<Map<String, Object>> subColumnas = new ArrayList<Map<String, Object>>();
            column = new HashMap<String, Object>();
            column.put("text", "CUENTAS DE FUNCIÓN DEL GASTO");
            for (String s : cuentasFuncionGasto) {
                Map<String, Object> subcolumna = new HashMap<String, Object>();
                subcolumna.put("text", s);
                subcolumna.put("dataIndex", "amount" + i++);
                subcolumna.put("width", 105);
                subcolumna.put("sortable", false);
                subcolumna.put("tdCls", "align-right-td");
                subcolumna.put("summaryType", "sum");
                subColumnas.add(subcolumna);
            }
            column.put("columns", subColumnas);
            columns.add(column);
        }

        // cuentas de orden
        if (!cuentasOrden.isEmpty()) {
            List<Map<String, Object>> subColumnas = new ArrayList<Map<String, Object>>();
            column = new HashMap<String, Object>();
            column.put("text", "CUENTAS DE ORDEN");
            for (String s : cuentasOrden) {
                Map<String, Object> subcolumna = new HashMap<String, Object>();
                subcolumna.put("text", s);
                subcolumna.put("dataIndex", "amount" + i++);
                subcolumna.put("width", 105);
                subcolumna.put("sortable", false);
                subcolumna.put("tdCls", "align-right-td");
                subcolumna.put("summaryType", "sum");
                subColumnas.add(subcolumna);
            }
            column.put("columns", subColumnas);
            columns.add(column);
        }


        this.setLineasDiarioSimplificado(listaLineas);
        this.getMetaData().put("root", "lineasDiarioSimplificado");
        this.getMetaData().put("idProperty", "numeroCorrelativo");
        this.getMetaData().put("totalProperty", "totalCount");
        this.getMetaData().put("successProperty", "success");
        this.getMetaData().put("fields", fields);
        this.getMetaData().put("columns", columns);
        this.setSuccess(true);
        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "listAsientosContables";
    }

    public String listTotalComprasPorPeriodo() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        Contacto c = factory.getContactoDAO().findById(((Contacto) user).getIdContacto());

        for (LibroRegistroCompras l : c.getEmpresaCliente().getLibrosRegistroCompras()) {
            BigDecimal total = new BigDecimal("0.00");
            total = total.setScale(2, RoundingMode.HALF_EVEN);

            for (DetalleLibroRegistroCompras dlrc : l.getDetallesLibroRegistroCompras()) {
                total = total.add(dlrc.getComprobanteCompra().getImporteTotal());
            }
            SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
            Map<String, Object> m = new LinkedHashMap<String, Object>();
            m.put("periodo", sdf.format(l.getPeriodo()));
            m.put("total", total);
            this.getTotalesPeriodosCompras().add(m);
        }


        /*
         if (this.getRango() == 1) {
            
         } else if (this.getRango() == 2) {
            
         } else if (this.getRango() == 3) {
            
         }*/



        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "listTotalComprasPorPeriodo";
    }

    public String listTotalVentasPorPeriodo() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        Contacto c = factory.getContactoDAO().findById(((Contacto) user).getIdContacto());

        for (LibroRegistroVentas l : c.getEmpresaCliente().getLibrosRegistroVentas()) {
            BigDecimal total = new BigDecimal("0.00");
            total = total.setScale(2, RoundingMode.HALF_EVEN);

            for (DetalleLibroRegistroVentas dlrv : l.getDetallesLibroRegistroVentas()) {
                total = total.add(dlrv.getComprobanteVenta().getImporteTotal());
            }
            SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
            Map<String, Object> m = new LinkedHashMap<String, Object>();
            m.put("periodo", sdf.format(l.getPeriodo()));
            m.put("total", total);
            this.getTotalesPeriodosVentas().add(m);
        }


        /*
         if (this.getRango() == 1) {
            
         } else if (this.getRango() == 2) {
            
         } else if (this.getRango() == 3) {
            
         }*/



        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "listTotalVentasPorPeriodo";
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(int idLibro) {
        this.idLibro = idLibro;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<DetalleLibroRegistroVentasSerializable> getListaDetallesRVSrl() {
        return listaDetallesRVSrl;
    }

    public void setListaDetallesRVSrl(List<DetalleLibroRegistroVentasSerializable> listaDetallesRVSrl) {
        this.listaDetallesRVSrl = listaDetallesRVSrl;
    }

    public List<DetalleLibroRegistroComprasSerializable> getListaDetallesRCSrl() {
        return listaDetallesRCSrl;
    }

    public void setListaDetallesRCSrl(List<DetalleLibroRegistroComprasSerializable> listaDetallesRCSrl) {
        this.listaDetallesRCSrl = listaDetallesRCSrl;
    }

    @Override
    public void setUser(Object user) {
        this.user = user;
    }

    public List<Map<String, Object>> getLineasDiarioSimplificado() {
        return lineasDiarioSimplificado;
    }

    public void setLineasDiarioSimplificado(List<Map<String, Object>> lineasDiarioSimplificado) {
        this.lineasDiarioSimplificado = lineasDiarioSimplificado;
    }

    public Map<String, Object> getMetaData() {
        return metaData;
    }

    public void setMetaData(Map<String, Object> metaData) {
        this.metaData = metaData;
    }

    public List<Map<String, Object>> getTotalesPeriodosCompras() {
        return totalesPeriodosCompras;
    }

    public void setTotalesPeriodosCompras(List<Map<String, Object>> totalesPeriodosCompras) {
        this.totalesPeriodosCompras = totalesPeriodosCompras;
    }

    public List<Map<String, Object>> getTotalesPeriodosVentas() {
        return totalesPeriodosVentas;
    }

    public void setTotalesPeriodosVentas(List<Map<String, Object>> totalesPeriodosVentas) {
        this.totalesPeriodosVentas = totalesPeriodosVentas;
    }
}
