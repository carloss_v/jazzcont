/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.controller.cliente;

import com.jazzcontadores.extra.JCConstants;
import com.jazzcontadores.model.dao.CompradorDAO;
import com.jazzcontadores.model.dao.LibroRegistroVentasDAO;
import com.jazzcontadores.model.entities.Comprador;
import com.jazzcontadores.model.entities.Contacto;
import com.jazzcontadores.model.entities.DetalleComprobanteVenta;
import com.jazzcontadores.model.entities.DetalleLibroRegistroVentas;
import com.jazzcontadores.model.entities.LibroRegistroVentas;
import com.jazzcontadores.model.entities.ProductoVentas;
import com.jazzcontadores.model.entities.TipoComprobantePagoODocumento;
import com.jazzcontadores.model.entities.TipoDocumentoIdentidad;
import com.jazzcontadores.util.DAOFactory;
import com.jazzcontadores.util.HibernateUtil;
import com.jazzcontadores.util.UserAware;
import com.opensymphony.xwork2.ActionSupport;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Venegas
 */
public class DetalleLibroRegistroVentasAction extends ActionSupport implements UserAware {

    Object user;
    private boolean nuevoComprador;
    private String tipoOperacion;
    private DetalleLibroRegistroVentas detalleLRV;
    private List<TipoComprobantePagoODocumento> tiposComprobantes = new ArrayList<TipoComprobantePagoODocumento>();
    private List<TipoDocumentoIdentidad> tiposDocumentos = new ArrayList<TipoDocumentoIdentidad>();

    public String add() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        // llenar la lista de tipos de comprobantes en la vista        
        this.setTiposComprobantes(factory.getTipoComprobantePagoODocumentoDAO().findAll());
        // llenar la lista de tipo de documentos en la vista
        this.setTiposDocumentos(factory.getTipoDocumentoIdentidadDAO().findAll());

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "add";
    }

    public String save() throws Exception {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        LibroRegistroVentasDAO libroVentasDAO = factory.getLibroRegistroVentasDAO();
        CompradorDAO compradorDAO = factory.getCompradorDAO();

        // buscamos libro de registro de ventas existente, sino creamos uno.
        Calendar cPeriodo = new GregorianCalendar();
        cPeriodo.setTime(this.getDetalleLRV().getComprobanteVenta().getFechaEmision());
        // para los periodos de los libros, el día es 1 siempre
        cPeriodo.set(Calendar.DAY_OF_MONTH, 1);

        LibroRegistroVentas libroExistente = libroVentasDAO.findByPeriodo(((Contacto) user).getEmpresaCliente().getRuc(), cPeriodo.getTime());

        try {
            if (libroExistente == null) {
                Calendar cFechaFin = (Calendar) cPeriodo.clone();
                cFechaFin.set(Calendar.DAY_OF_MONTH, cFechaFin.getActualMaximum(Calendar.DAY_OF_MONTH));

                LibroRegistroVentas libroRVNuevo = new LibroRegistroVentas();
                libroRVNuevo.setPeriodo(cPeriodo.getTime());
                libroRVNuevo.setFechaInicio(cPeriodo.getTime());
                libroRVNuevo.setFechaFin(cFechaFin.getTime());
                libroRVNuevo.setEstaCerrado(false);
                libroRVNuevo.setEmpresaCliente(factory.getEmpresaClienteDAO().findById(((Contacto) user).getEmpresaCliente().getIdEmpresaCliente()));

                libroVentasDAO.makePersistent(libroRVNuevo);

                if (this.getDetalleLRV().getComprobanteVenta().getComprador() != null
                        && this.isNuevoComprador()) {
                    compradorDAO.makePersistent(this.getDetalleLRV().getComprobanteVenta().getComprador());
                }

                this.getDetalleLRV().setLibroRegistroVentas(libroRVNuevo); // no se puede obviar
                this.getDetalleLRV().setFechaHoraRegistro(new Date());
                this.getDetalleLRV().setNumeroCorrelativo(1); // libro nuevo, primer detalle

                libroRVNuevo.getDetallesLibroRegistroVentas().add(this.getDetalleLRV());

            } else {
                if (libroExistente.isEstaCerrado()) {
                    return "libroCerrado";
                }

                if (this.getDetalleLRV().getComprobanteVenta().getComprador() != null
                        && this.isNuevoComprador()) {
                    compradorDAO.makePersistent(this.getDetalleLRV().getComprobanteVenta().getComprador());
                }

                this.getDetalleLRV().setLibroRegistroVentas(libroExistente); // no se puede obviar
                this.getDetalleLRV().setFechaHoraRegistro(new Date());

                // reordenamos la lista de detalles
                Date ultimaFechaRegistrada = libroExistente.getDetallesLibroRegistroVentas()
                        .get(libroExistente.getDetallesLibroRegistroVentas().size() - 1)
                        .getComprobanteVenta().getFechaEmision();
                // si la fecha de emision del comprobante es anterior a la fecha del último detalle ordenado
                if (this.getDetalleLRV().getComprobanteVenta().getFechaEmision().before(ultimaFechaRegistrada)) {
                    this.getDetalleLRV().setNumeroCorrelativo(1000000);// se pone al último de la lista
                    libroExistente.getDetallesLibroRegistroVentas().add(this.getDetalleLRV());
                    // reordenamos la lista
                    Collections.sort(libroExistente.getDetallesLibroRegistroVentas(), new Comparator<DetalleLibroRegistroVentas>() {
                        @Override
                        public int compare(DetalleLibroRegistroVentas d1, DetalleLibroRegistroVentas d2) {
                            return d1.getComprobanteVenta().getFechaEmision().compareTo(d2.getComprobanteVenta().getFechaEmision());
                        }
                    });
                    // establecemos el numero correlativo
                    for (int i = 0; i < libroExistente.getDetallesLibroRegistroVentas().size(); i++) {
                        // se reordena la lista de detalles
                        libroExistente.getDetallesLibroRegistroVentas().get(i).setNumeroCorrelativo(i + 1);
                    }
                } else {
                    // nos aseguramos que el tamaño sea mayor que 0 para no generar una excepcion
                    if (libroExistente.getDetallesLibroRegistroVentas().size() > 0) {
                        // le asignamos el último número correlativo más 1
                        DetalleLibroRegistroVentas d = libroExistente.getDetallesLibroRegistroVentas().get(libroExistente.getDetallesLibroRegistroVentas().size() - 1);
                        int ultimoNCorrelativo = d.getNumeroCorrelativo();
                        this.getDetalleLRV().setNumeroCorrelativo(ultimoNCorrelativo + 1);
                    } else {
                        this.getDetalleLRV().setNumeroCorrelativo(1);
                    }

                    libroExistente.getDetallesLibroRegistroVentas().add(this.getDetalleLRV());
                }

            }

            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();

        } catch (Exception e) {
            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().rollback();
            System.err.println(e + ": " + e.getMessage());
            throw e;
            //return ERROR;

        }

        return "save";
    }

    public void validateSave() {
        // se inyecta el cliente antes de hacer las validaciones
        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);

        // llenar la lista de tipos de comprobantes en la vista        
        this.setTiposComprobantes(factory.getTipoComprobantePagoODocumentoDAO().findAll());
        // llenar la lista de tipo de documentos en la vista
        this.setTiposDocumentos(factory.getTipoDocumentoIdentidadDAO().findAll());

        // validaciones        
        if (getDetalleLRV().getComprobanteVenta().getFechaEmision() == null) {
            addActionError("Debe especificar la fecha de emisión del comprobante.");
        }
        if (getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() < 0) {
            addActionError("Debe especificar el tipo de comprobante (Tabla 10).");
        }
        if (!getDetalleLRV().getComprobanteVenta().getSerie().trim().matches("-|\\w{1,20}")) {
            addActionError("El formato de la serie del comprobante es incorrecto.");
        }
        if (!getDetalleLRV().getComprobanteVenta().getNumero().trim().equals("")
                && !getDetalleLRV().getComprobanteVenta().getNumero().trim().matches("^\\d{1,20}")) {
            addActionError("El formato del número de comprobante es incorrecto.");
        }

        // verificar existencia de comprobante
        if (factory.getComprobanteVentaDAO().findByEmpresaSerieNumero(
                ((Contacto) user).getEmpresaCliente().getRuc(),
                this.getDetalleLRV().getComprobanteVenta().getSerie(),
                this.getDetalleLRV().getComprobanteVenta().getNumero()) != null) {
            addActionError("Un comprobante con esa serie y número ya se ha registrado en el sistema.");
        }

        // verificar existencia de comprador
        if (getDetalleLRV().getComprobanteVenta().getComprador().getTipoDocumentoIdentidad().getNumero() != null
                && getDetalleLRV().getComprobanteVenta().getComprador().getNumeroDocumentoIdentidad() != null) {
            Comprador comprador = factory.getCompradorDAO().findByTipoDocumentoYNumeroDocumento(
                    ((Contacto) user).getEmpresaCliente().getRuc(),
                    this.getDetalleLRV().getComprobanteVenta().getComprador().getTipoDocumentoIdentidad().getNumero(),
                    this.getDetalleLRV().getComprobanteVenta().getComprador().getNumeroDocumentoIdentidad());
            if (comprador != null) {
                if (this.isNuevoComprador()) {
                    addActionError("El comprador a registrar ya existe en la base de datos.");
                } else {
                    getDetalleLRV().getComprobanteVenta().setComprador(comprador);
                }
            }
        }

        // =========================================================
        // reglas del formato de libros electronicos *********************
        // fecha de emisión
        if (getDetalleLRV().getComprobanteVenta().getFechaEmision() != null) {
            if (getDetalleLRV().getComprobanteVenta().getFechaEmision().after(new Date())) {
                addActionError("La fecha de emisión es incorrecta. No se puede registrar fechas futuras en el sistema.");
            } else {
                // 1. Obligatorio, excepto cuando el campo 27 = '2'
                // 2. Menor o igual al periodo informado
                // 3. Menor o igual al periodo señalado en el campo 1.
                getDetalleLRV().setEstadoOportunidadDeAnotacion("1");
            }
        }

        // fecha de vencimiento o pago
        if (getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 14) {
            if (getDetalleLRV().getComprobanteVenta().getFechaVencimiento() == null) {
                addActionError("Debe especificar la fecha de vencimiento o pago para el tipo de comprobante seleccionado.");
            }
        }

        // serie 
        if (getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() != null
                && getDetalleLRV().getComprobanteVenta().getNumero() != null) {
            // 3. Si campo 5 = '01', '03', '04', '07', '08' => longitud 4 o 6, para el resto longitud 20
            if (getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 1
                    || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 3
                    || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 4
                    || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 7
                    || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 8) {
                if (!getDetalleLRV().getComprobanteVenta().getSerie().matches("-|\\w{1,4}")) {
                    addActionError("El número de serie para el tipo de comprobante seleccionado debe tener hasta 4 dígitos.");
                }
            } else if (getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 0) {
                // 4. Si campo 5 = '00' => registrar '-'
                if (!getDetalleLRV().getComprobanteVenta().getSerie().matches("-")) {
                    addActionError("El número de serie para el tipo de comprobante seleccionado debe ser '-'.");
                }
            }
        }

        // valor facturado de la exportación        
        if (getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion() != null) {
            // 3. Acepta negativos sólo si campo 5 = '07' o '87' 0 '97'
            if (getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 7
                    || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 87
                    || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 97) {
                if (getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion().precision() > 14
                        || getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion().scale() > 2) {
                    addActionError("El formato del valor facturado de la exportación es incorrecto.");
                }
            } else {
                if (getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion().compareTo(BigDecimal.ZERO) < 0
                        || getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion().precision() > 14
                        || getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion().scale() > 2) {
                    addActionError("El formato del valor facturado de la exportación es incorrecto.");
                }
            }
        } else {
            // de no existir registrar '0.00'
            //getDetalleLRV().getComprobanteVenta().setValorFacturadoExportacion(new BigDecimal("0.00"));
        }

        // otros tributos y cargos se considera en los 3 tipos de operacion
        if (this.getDetalleLRV().getComprobanteVenta().getOtrosTributos() != null) {
            if (this.getDetalleLRV().getComprobanteVenta().getOtrosTributos().precision() > 14
                    || this.getDetalleLRV().getComprobanteVenta().getOtrosTributos().scale() > 2) {
                addActionError("El formato de Otros tributos y cargos es incorrecto.");
            }
        } else {
            //addActionError("Debe especificar el valor de Otros tributos y cargos.");
        }

        // tipo de cambio
        if (this.getDetalleLRV().getTipoCambio() != null) {
            if (this.getDetalleLRV().getTipoCambio().precision() > 4
                    || this.getDetalleLRV().getTipoCambio().scale() > 3) {
                addActionError("El formato del tipo de cambio es incorrecto.");
            }
        } else {
            //
        }


        // base imponible de la operación gravada
        if (this.getTipoOperacion().equals("gravada")) {

            this.getDetalleLRV().getComprobanteVenta().setTotalOperacionExonerada(null);
            this.getDetalleLRV().getComprobanteVenta().setTotalOperacionInafecta(null);

            BigDecimal total = new BigDecimal("0.00");
            total = total.setScale(2, RoundingMode.HALF_EVEN);
            if (!this.getDetalleLRV().getComprobanteVenta().getDetallesComprobanteVenta().isEmpty()) {

                for (Iterator<DetalleComprobanteVenta> it = this.getDetalleLRV().getComprobanteVenta().getDetallesComprobanteVenta().iterator(); it.hasNext();) {
                    DetalleComprobanteVenta d = it.next();

                    if (d.getProductoVentas().getIdProductoVentas() == 0) {
                        // crear nueva referencia para registrar nuevo producto
                        ProductoVentas pv = new ProductoVentas();
                        pv.setNombre(d.getProductoVentas().getNombre());
                        pv.setPrecio(d.getProductoVentas().getPrecio());
                        d.setProductoVentas(pv);
                    }
                    BigDecimal subtotal = d.getProductoVentas().getPrecio().multiply(new BigDecimal(d.getCantidad()));
                    subtotal = subtotal.setScale(2, RoundingMode.HALF_EVEN);
                    d.setSubtotal(subtotal);
                    d.setComprobanteVenta(this.getDetalleLRV().getComprobanteVenta());
                    d.setPrecioUnitario(d.getProductoVentas().getPrecio()); // se copia el precio
                    total = total.add(d.getSubtotal());
                }

                BigDecimal base = total.multiply(JCConstants.BASE);
                BigDecimal igv = total.multiply(JCConstants.IGV);

                base = base.setScale(2, RoundingMode.HALF_EVEN);
                igv = igv.setScale(2, RoundingMode.HALF_EVEN);

                this.getDetalleLRV().getComprobanteVenta().setBaseImponibleOperacionGravada(base);
                this.getDetalleLRV().getComprobanteVenta().setIgv(igv);

            } else {
                // calcular en base a los campos introducidos manualmente
                if (this.getDetalleLRV().getComprobanteVenta().getImporteTotal() != null) { // para q no genere excepción
                    total = this.getDetalleLRV().getComprobanteVenta().getImporteTotal();
                    BigDecimal base = total.multiply(JCConstants.BASE);
                    BigDecimal igv = total.multiply(JCConstants.IGV);

                    base = base.setScale(2, RoundingMode.HALF_EVEN);
                    igv = igv.setScale(2, RoundingMode.HALF_EVEN);

                    this.getDetalleLRV().getComprobanteVenta().setBaseImponibleOperacionGravada(base);
                    this.getDetalleLRV().getComprobanteVenta().setIgv(igv);
                }
            }

            if (this.getDetalleLRV().getComprobanteVenta().getIsc() != null) {
                total = total.add(this.getDetalleLRV().getComprobanteVenta().getIsc());
            }
            if (this.getDetalleLRV().getComprobanteVenta().getOtrosTributos() != null) {
                total = total.add(this.getDetalleLRV().getComprobanteVenta().getOtrosTributos());
            }

            this.getDetalleLRV().getComprobanteVenta().setImporteTotal(total);


            // campos que pueden ser negativos
            if (this.getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 7
                    || this.getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 87
                    || this.getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 97) {
                // acepta negativos
                // base imponible
                if (this.getDetalleLRV().getComprobanteVenta().getBaseImponibleOperacionGravada() != null) {
                    if (this.getDetalleLRV().getComprobanteVenta().getBaseImponibleOperacionGravada().precision() > 14
                            || this.getDetalleLRV().getComprobanteVenta().getBaseImponibleOperacionGravada().scale() > 2) {
                        addActionError("El formato de la Base Imponible es incorrecto.");
                    }
                } else {
                    addActionError("Debe especificar el valor la Base Imponible.");
                }
                // igv
                if (this.getDetalleLRV().getComprobanteVenta().getIgv() != null) {
                    if (this.getDetalleLRV().getComprobanteVenta().getIgv().precision() > 14
                            || this.getDetalleLRV().getComprobanteVenta().getIgv().scale() > 2) {
                        addActionError("El formato del IGV es incorrecto.");
                    }
                } else {
                    addActionError("Debe especificar el valor del IGV.");
                }
                // isc
                if (this.getDetalleLRV().getComprobanteVenta().getIsc() != null) {
                    if (this.getDetalleLRV().getComprobanteVenta().getIsc().precision() > 14
                            || this.getDetalleLRV().getComprobanteVenta().getIsc().scale() > 2) {
                        addActionError("El formato del ISC es incorrecto.");
                    }
                } else {
                    //addActionError("Debe especificar el valor del ISC.");
                }

            } else {
                // no acepta negativos
                // base
                if (this.getDetalleLRV().getComprobanteVenta().getBaseImponibleOperacionGravada() != null) {
                    if (this.getDetalleLRV().getComprobanteVenta().getBaseImponibleOperacionGravada().compareTo(BigDecimal.ZERO) < 0
                            || this.getDetalleLRV().getComprobanteVenta().getBaseImponibleOperacionGravada().precision() > 14
                            || this.getDetalleLRV().getComprobanteVenta().getBaseImponibleOperacionGravada().scale() > 2) {
                        addActionError("El formato de la Base Imponible es incorrecto.");
                    }
                } else {
                    addActionError("Debe especificar el valor la Base Imponible.");
                }
                // igv
                if (this.getDetalleLRV().getComprobanteVenta().getIgv() != null) {
                    if (this.getDetalleLRV().getComprobanteVenta().getIgv().compareTo(BigDecimal.ZERO) < 0
                            || this.getDetalleLRV().getComprobanteVenta().getIgv().precision() > 14
                            || this.getDetalleLRV().getComprobanteVenta().getIgv().scale() > 2) {
                        addActionError("El formato del IGV es incorrecto.");
                    }
                } else {
                    addActionError("Debe especificar el valor del IGV.");
                }
                // isc
                if (this.getDetalleLRV().getComprobanteVenta().getIsc() != null) {
                    if (this.getDetalleLRV().getComprobanteVenta().getIsc().compareTo(BigDecimal.ZERO) < 0
                            || this.getDetalleLRV().getComprobanteVenta().getIsc().precision() > 14
                            || this.getDetalleLRV().getComprobanteVenta().getIsc().scale() > 2) {
                        addActionError("El formato del ISC es incorrecto.");
                    }
                } else {
                    //addActionError("Debe especificar el valor del ISC.");
                }

            }

        } else if (this.getTipoOperacion().equals("exonerada")) {

            this.getDetalleLRV().getComprobanteVenta().setBaseImponibleOperacionGravada(null);
            this.getDetalleLRV().getComprobanteVenta().setTotalOperacionInafecta(null);
            this.getDetalleLRV().getComprobanteVenta().setIsc(null);
            this.getDetalleLRV().getComprobanteVenta().setIgv(null);

            BigDecimal total = new BigDecimal("0.00");
            total = total.setScale(2, RoundingMode.HALF_EVEN);   
            
            // importe total de la operación exonerada
            if (getDetalleLRV().getComprobanteVenta().getTotalOperacionExonerada() != null) {
                // 3. Acepta negativos sólo si campo 5 = '07' o '87' 0 '97'
                if (getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 7
                        || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 87
                        || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 97) {
                    if (getDetalleLRV().getComprobanteVenta().getTotalOperacionExonerada().precision() > 14
                            || getDetalleLRV().getComprobanteVenta().getTotalOperacionExonerada().scale() > 2) {
                        addActionError("El formato del importe total de la operación exonerada es incorrecto.");
                    }
                } else {
                    if (getDetalleLRV().getComprobanteVenta().getTotalOperacionExonerada().compareTo(BigDecimal.ZERO) < 0
                            || getDetalleLRV().getComprobanteVenta().getTotalOperacionExonerada().precision() > 14
                            || getDetalleLRV().getComprobanteVenta().getTotalOperacionExonerada().scale() > 2) {
                        addActionError("El formato del importe total de la operación exonerada es incorrecto.");
                    }
                }                
                // se calcula el total del comprobante          
                total = total.add(getDetalleLRV().getComprobanteVenta().getTotalOperacionExonerada());
                if (getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion() != null) {
                    total = total.add(getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion());
                }
                if (getDetalleLRV().getComprobanteVenta().getOtrosTributos() != null) {
                    total = total.add(getDetalleLRV().getComprobanteVenta().getOtrosTributos());
                }
                
                this.getDetalleLRV().getComprobanteVenta().setImporteTotal(total);
                
            } else {
                addActionError("Debe especificar el total de la operación exonerada.");
            }
        } else if (this.getTipoOperacion().equals("inafecta")) {

            this.getDetalleLRV().getComprobanteVenta().setBaseImponibleOperacionGravada(null);
            this.getDetalleLRV().getComprobanteVenta().setTotalOperacionExonerada(null);
            this.getDetalleLRV().getComprobanteVenta().setIsc(null);
            this.getDetalleLRV().getComprobanteVenta().setIgv(null);

            BigDecimal total = new BigDecimal("0.00");
            total = total.setScale(2, RoundingMode.HALF_EVEN);   
            
            // importe total de la operación inafecta
            if (getDetalleLRV().getComprobanteVenta().getTotalOperacionInafecta() != null) {
                // 3. Acepta negativos sólo si campo 5 = '07' o '87' 0 '97'
                if (getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 7
                        || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 87
                        || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 97) {
                    if (getDetalleLRV().getComprobanteVenta().getTotalOperacionInafecta().precision() > 14
                            || getDetalleLRV().getComprobanteVenta().getTotalOperacionInafecta().scale() > 2) {
                        addActionError("El formato del importe total de la operación inafecta es incorrecto.");
                    }
                } else {
                    if (getDetalleLRV().getComprobanteVenta().getTotalOperacionInafecta().compareTo(BigDecimal.ZERO) < 0
                            || getDetalleLRV().getComprobanteVenta().getTotalOperacionInafecta().precision() > 14
                            || getDetalleLRV().getComprobanteVenta().getTotalOperacionInafecta().scale() > 2) {
                        addActionError("El formato del importe total de la operación inafecta es incorrecto.");
                    }
                }
                
                // se calcula el total del comprobante          
                total = total.add(getDetalleLRV().getComprobanteVenta().getTotalOperacionInafecta());
                if (getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion() != null) {
                    total = total.add(getDetalleLRV().getComprobanteVenta().getValorFacturadoExportacion());
                }
                if (getDetalleLRV().getComprobanteVenta().getOtrosTributos() != null) {
                    total = total.add(getDetalleLRV().getComprobanteVenta().getOtrosTributos());
                }
                
                this.getDetalleLRV().getComprobanteVenta().setImporteTotal(total);
                
            } else {
                addActionError("Debe especificar el importe total de la operación inafecta.");
            }
        }

        // comprador 
        // 1. Obligatorio, excepto cuando 
        // a. campo 5 = '00','03','05','06','07','08','11','12','13','14','15','16','18','19','23','26','28','30','34','35','36','37','55','56','87' y '88' o 
        if ((getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 0
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 3
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 5
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 6
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 7
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 8
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 11
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 12
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 13
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 14
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 15
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 16
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 18
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 19
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 23
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 26
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 28
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 30
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 34
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 35
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 36
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 37
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 55
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 56
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 87
                || getDetalleLRV().getComprobanteVenta().getTipoComprobantePagoODocumento().getNumero() == 88)
                && getDetalleLRV().getComprobanteVenta().getImporteTotal().compareTo(new BigDecimal("700.00")) <= 0) {
            // es opcional
            // si alguno de los campos no tiene su valor vacío
            if (!getDetalleLRV().getComprobanteVenta().getComprador().getTipoDocumentoIdentidad().getNumero().trim().equals("-1")
                    || !getDetalleLRV().getComprobanteVenta().getComprador().getNumeroDocumentoIdentidad().trim().equals("")
                    || !getDetalleLRV().getComprobanteVenta().getComprador().getRazonSocialONombres().trim().equals("")) {
                // verificar formación correcta
                if (getDetalleLRV().getComprobanteVenta().getComprador().getTipoDocumentoIdentidad().getNumero().trim().equals("-1")) {
                    addActionError("Debe especificar el tipo de documento de identidad del comprador.");
                }
                if (!getDetalleLRV().getComprobanteVenta().getComprador().getNumeroDocumentoIdentidad().trim().matches("-|\\w{1,15}")) {
                    addActionError("El formato del numero de documento de identidad del comprador es incorrecto.");
                }
                if (!getDetalleLRV().getComprobanteVenta().getComprador().getRazonSocialONombres().trim().matches("-|.{1,60}")) {
                    addActionError("El formato de la razón social del comprador es incorrecto.");
                }
            } else {
                // si todos los campos tienen su valor vacío
                this.getDetalleLRV().getComprobanteVenta().setComprador(null);
            }
        } else {
            // es obligatorio
            if (getDetalleLRV().getComprobanteVenta().getComprador().getTipoDocumentoIdentidad().getNumero().trim().equals("-1")) {
                addActionError("Debe especificar el tipo de documento de identidad del comprador.");
            }
            if (!getDetalleLRV().getComprobanteVenta().getComprador().getNumeroDocumentoIdentidad().trim().matches("-|\\w{1,15}")) {
                addActionError("El formato del numero de documento de identidad del comprador es incorrecto.");
            }
            if (!getDetalleLRV().getComprobanteVenta().getComprador().getRazonSocialONombres().trim().matches("-|.{1,60}")) {
                addActionError("El formato de la razón social del comprador es incorrecto.");
            }
        }


        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
    }

    public boolean isNuevoComprador() {
        return nuevoComprador;
    }

    public void setNuevoComprador(boolean nuevoComprador) {
        this.nuevoComprador = nuevoComprador;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public DetalleLibroRegistroVentas getDetalleLRV() {
        return detalleLRV;
    }

    public void setDetalleLRV(DetalleLibroRegistroVentas detalleLRV) {
        this.detalleLRV = detalleLRV;
    }

    public List<TipoComprobantePagoODocumento> getTiposComprobantes() {
        return tiposComprobantes;
    }

    public void setTiposComprobantes(List<TipoComprobantePagoODocumento> tiposComprobantes) {
        this.tiposComprobantes = tiposComprobantes;
    }

    public List<TipoDocumentoIdentidad> getTiposDocumentos() {
        return tiposDocumentos;
    }

    public void setTiposDocumentos(List<TipoDocumentoIdentidad> tiposDocumentos) {
        this.tiposDocumentos = tiposDocumentos;
    }

    @Override
    public void setUser(Object user) {
        this.user = user;
    }
}
