/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzcontadores.controller.cliente;

import com.jazzcontadores.model.dao.EmpresaClienteDAO;
import com.jazzcontadores.model.entities.Comprador;
import com.jazzcontadores.model.entities.Contacto;
import com.jazzcontadores.model.entities.ProductoCompras;
import com.jazzcontadores.model.entities.ProductoVentas;
import com.jazzcontadores.model.entities.Proveedor;
import com.jazzcontadores.model.entitiessrl.CompradorSerializable;
import com.jazzcontadores.model.entitiessrl.ProductoComprasSerializable;
import com.jazzcontadores.model.entitiessrl.ProductoVentasSerializable;
import com.jazzcontadores.model.entitiessrl.ProveedorSerializable;
import com.jazzcontadores.util.DAOFactory;
import com.jazzcontadores.util.HibernateUtil;
import com.jazzcontadores.util.UserAware;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Venegas
 */
public class ClienteAjaxAction extends ActionSupport implements UserAware {
    
    Object user;
    private String param;
    private String paramRetorno;
    private String mensaje; // mensaje para las acciones de edit 
    private String term;
    private List<ProveedorSerializable> listaProveedoresSrl = new ArrayList<ProveedorSerializable>();
    private List<CompradorSerializable> listaCompradoresSrl = new ArrayList<CompradorSerializable>();
    private List<ProductoVentasSerializable> listaProductosVentasSrl = new ArrayList<ProductoVentasSerializable>();
    private List<ProductoComprasSerializable> listaProductosComprasSrl = new ArrayList<ProductoComprasSerializable>();
    
    public String editEmailSecundarioContacto() {
        String nuevoParam = this.getParam().trim();

        if (!nuevoParam.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            this.setMensaje("error");
            return ERROR;
        }

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);  
        
        Contacto c = factory.getContactoDAO().findById(((Contacto) user).getIdContacto());
        c.setEmailSecundario(nuevoParam);        

        // devolver el nuevo valor escrito en la BD
        this.setParamRetorno(c.getEmailSecundario());
        this.setMensaje("ok");

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        // se actualiza el objeto de la sesión
        ((Contacto) user).setEmailSecundario(c.getEmailSecundario());
        return "edit";
    }
    
    public String editEmailFacebookContacto() {
        String nuevoParam = this.getParam().trim();

        if (!nuevoParam.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            this.setMensaje("error");
            return ERROR;
        }

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);  
        
        Contacto c = factory.getContactoDAO().findById(((Contacto) user).getIdContacto());
        c.setEmailFacebook(nuevoParam);       

        // devolver el nuevo valor escrito en la BD
        this.setParamRetorno(c.getEmailFacebook());
        this.setMensaje("ok");

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        // se actualiza el objeto de la sesión
        ((Contacto) user).setEmailFacebook(c.getEmailFacebook());
        return "edit";
    }
    
    public String listProveedoresByCliente() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        
        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        List<Proveedor> lista = empresaDAO.findProveedoresByTerm(((Contacto) user).getEmpresaCliente().getRuc(), this.getTerm());

        for (Proveedor p : lista) {
            ProveedorSerializable pSrl = new ProveedorSerializable();
            pSrl.setIdProveedor(p.getIdProveedor());
            pSrl.setRazonSocial(p.getRazonSocial());
            pSrl.setNumeroDocumentoIdentidad(p.getNumeroDocumentoIdentidad());
            pSrl.setTipoDocNumero(p.getTipoDocumentoIdentidad().getNumero());
            pSrl.setTipoDocDescripcion(p.getTipoDocumentoIdentidad().getDescripcion());
            this.getListaProveedoresSrl().add(pSrl);
        }

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "listProveedoresByCliente";
    }

    public String listCompradoresByCliente() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        
        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        List<Comprador> lista = empresaDAO.findCompradoresByTerm(((Contacto) user).getEmpresaCliente().getRuc(), this.getTerm());

        for (Comprador c : lista) {
            CompradorSerializable cSrl = new CompradorSerializable();
            cSrl.setIdComprador(c.getIdComprador());
            cSrl.setRazonSocialONombres(c.getRazonSocialONombres());
            cSrl.setNumeroDocumentoIdentidad(c.getNumeroDocumentoIdentidad());
            cSrl.setTipoDocNumero(c.getTipoDocumentoIdentidad().getNumero());
            cSrl.setTipoDocDescripcion(c.getTipoDocumentoIdentidad().getDescripcion());
            this.getListaCompradoresSrl().add(cSrl);
        }

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "listCompradoresByCliente";
    }

    public String listProductosVentasByCliente() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        
        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        List<ProductoVentas> lista = empresaDAO.findProductosVentasByTerm(((Contacto) user).getEmpresaCliente().getRuc(), this.getTerm());

        for (ProductoVentas p : lista) {
            ProductoVentasSerializable pSrl = new ProductoVentasSerializable();
            pSrl.setIdProductoVentas(p.getIdProductoVentas());
            pSrl.setStock(p.getStock());
            pSrl.setNombre(p.getNombre());
            pSrl.setPrecio(p.getPrecio());
            pSrl.setUnidadDeMedida(p.getUnidadDeMedida());
            this.getListaProductosVentasSrl().add(pSrl);
        }

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "listProductosVentasByCliente";
    }

    public String listProductosComprasByCliente() {

        HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();

        DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
        EmpresaClienteDAO empresaDAO = factory.getEmpresaClienteDAO();

        List<ProductoCompras> lista = empresaDAO.findProductosComprasByTerm(((Contacto) user).getEmpresaCliente().getRuc(), this.getTerm());

        for (ProductoCompras p : lista) {
            ProductoComprasSerializable pSrl = new ProductoComprasSerializable();
            pSrl.setIdProductoCompras(p.getIdProductoCompras());
            pSrl.setStock(p.getStock());
            pSrl.setNombre(p.getNombre());
            pSrl.setPrecio(p.getPrecio());
            pSrl.setUnidadDeMedida(p.getUnidadDeMedida());
            this.getListaProductosComprasSrl().add(pSrl);
        }

        HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        return "listProductosComprasByCliente";
    }

    public String getParamRetorno() {
        return paramRetorno;
    }

    public void setParamRetorno(String paramRetorno) {
        this.paramRetorno = paramRetorno;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    @Override
    public void setUser(Object user) {
        this.user = user;
    }

    public List<ProveedorSerializable> getListaProveedoresSrl() {
        return listaProveedoresSrl;
    }

    public void setListaProveedoresSrl(List<ProveedorSerializable> listaProveedoresSrl) {
        this.listaProveedoresSrl = listaProveedoresSrl;
    }

    public List<CompradorSerializable> getListaCompradoresSrl() {
        return listaCompradoresSrl;
    }

    public void setListaCompradoresSrl(List<CompradorSerializable> listaCompradoresSrl) {
        this.listaCompradoresSrl = listaCompradoresSrl;
    }

    public List<ProductoVentasSerializable> getListaProductosVentasSrl() {
        return listaProductosVentasSrl;
    }

    public void setListaProductosVentasSrl(List<ProductoVentasSerializable> listaProductosVentasSrl) {
        this.listaProductosVentasSrl = listaProductosVentasSrl;
    }

    public List<ProductoComprasSerializable> getListaProductosComprasSrl() {
        return listaProductosComprasSrl;
    }

    public void setListaProductosComprasSrl(List<ProductoComprasSerializable> listaProductosComprasSrl) {
        this.listaProductosComprasSrl = listaProductosComprasSrl;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }
}
