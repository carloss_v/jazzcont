<%-- 
    Document   : ver-calendario-pagos
    Created on : 06/08/2013, 08:46:56 PM
    Author     : Venegas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>        
        <title>App | Jazz Contadores</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/css/style.css"/>">        
        <link rel="shortcut icon" href="<s:url value="/favicon1.ico"/>">
        <link rel="icon" type="image/ico" href="<s:url value="/favicon1.ico"/>">

        <link type="text/css" href="<s:url value="/css/custom-theme/jquery-ui-1.9.1.custom.min.css"/>" rel="stylesheet" />	
        <script type="text/javascript" src="<s:url value="/js/jquery-1.7.2.min.js"/>"></script>        
        <script type="text/javascript" src="<s:url value="/js/jquery-ui-1.9.1.custom.min.js"/>"></script> 

        <script type="text/javascript" src="<s:url value="/js/jquery.placeholder.min.js"/>"></script> 
        <script type="text/javascript" src="<s:url value="/js/scripts.js"/>"></script>

        <!-- FullCalendar -->
        <link rel="stylesheet" type="text/css" href="<s:url value="/css/fullcalendar.css"/>" />
        <script type="text/javascript" src="<s:url value="/js/fullcalendar.min.js"/>"></script>

        <script type="text/javascript"> 
            $(function() {
                
                // Toggle the dropdown menu's
                $(".dropdown .button").on('click', function () {
                    $(this).parent().find('.dropdown-slider').slideToggle('fast');
                    $(this).find('span.toggle').toggleClass('active');
                    return false;
                });
                
                // Close open dropdown slider/s by clicking elsewhwere on page
                $(document).on('click', function (e) {            
                    if (!$(e.target).is(".dropdown .dropdown-slider a span")) {
                        $('.dropdown-slider').slideUp();
                        $('span.toggle').removeClass('active');
                    }
                }); // END document.bind
                //************** 
                
                $("#calendar").fullCalendar({
                    //theme: true
                    events: [                        
                        {
                            title  : 'Computer Trujillo E.I.R.L',
                            start  : '2013-09-12'
                        },
                        {
                            title  : 'Empresa Test 2 S.A.C',
                            start  : '2013-08-12'
                        },
                        {
                            title  : 'Perú Trabaja S.A.C.',
                            start  : '2013-09-12'
                        },
                        {
                            title  : 'Upao test SAC',
                            start  : '2013-09-12'
                        },
                        {
                            title  : 'Empresa Test 3 S.A.',
                            start  : '2013-09-13'
                        },
                        {
                            title  : 'Empresa Test PP2 EIRL',
                            start  : '2013-09-14'                            
                        },
                        {
                            title  : 'Constructora S.A',
                            start  : '2013-09-15'
                        },                        
                        {
                            title  : 'Empresa Test S.A.',
                            start  : '2013-09-08'                            
                        },{
                            title  : 'Test EIRL',
                            start  : '2013-09-08'
                        },
                        
                        {
                            title  : 'Computer Trujillo E.I.R.L',
                            start  : '2013-10-12'
                        },
                        {
                            title  : 'Empresa Test 2 S.A.C',
                            start  : '2013-10-12'
                        },
                        {
                            title  : 'Perú Trabaja S.A.C.',
                            start  : '2013-10-12'
                        },
                        {
                            title  : 'Upao test SAC',
                            start  : '2013-10-12'
                        },
                        {
                            title  : 'Empresa Test 3 S.A.',
                            start  : '2013-10-13'
                        },
                        {
                            title  : 'Empresa Test PP2 EIRL',
                            start  : '2013-10-14'                            
                        },
                        {
                            title  : 'Constructora S.A',
                            start  : '2013-10-15'
                        },                        
                        {
                            title  : 'Empresa Test S.A.',
                            start  : '2013-10-08'                            
                        },{
                            title  : 'Test EIRL',
                            start  : '2013-10-08'
                        },
                        
                        {
                            title  : 'Computer Trujillo E.I.R.L',
                            start  : '2013-11-12'
                        },
                        {
                            title  : 'Empresa Test 2 S.A.C',
                            start  : '2013-11-12'
                        },
                        {
                            title  : 'Perú Trabaja S.A.C.',
                            start  : '2013-11-12'
                        },
                        {
                            title  : 'Upao test SAC',
                            start  : '2013-11-12'
                        },
                        {
                            title  : 'Empresa Test 3 S.A.',
                            start  : '2013-11-13'
                        },
                        {
                            title  : 'Empresa Test PP2 EIRL',
                            start  : '2013-11-14'                            
                        },
                        {
                            title  : 'Constructora S.A',
                            start  : '2013-11-15'
                        },                        
                        {
                            title  : 'Empresa Test S.A.',
                            start  : '2013-11-08'                            
                        },{
                            title  : 'Test EIRL',
                            start  : '2013-11-08'
                        },
                        
                        {
                            title  : 'Computer Trujillo E.I.R.L',
                            start  : '2013-12-12'
                        },
                        {
                            title  : 'Empresa Test 2 S.A.C',
                            start  : '2013-12-12'
                        },
                        {
                            title  : 'Perú Trabaja S.A.C.',
                            start  : '2013-12-12'
                        },
                        {
                            title  : 'Upao test SAC',
                            start  : '2013-12-12'
                        },
                        {
                            title  : 'Empresa Test 3 S.A.',
                            start  : '2013-12-13'
                        },
                        {
                            title  : 'Empresa Test PP2 EIRL',
                            start  : '2013-12-14'                            
                        },
                        {
                            title  : 'Constructora S.A',
                            start  : '2013-12-15'
                        },                        
                        {
                            title  : 'Empresa Test S.A.',
                            start  : '2013-12-08'                            
                        },{
                            title  : 'Test EIRL',
                            start  : '2013-12-08'
                        }
                    ]
                });                    
                      
            }); 
        </script>

    </head>
    <body>
        <header>
            <div id="header_content">
                <div id="logo"><a href="<%= request.getContextPath()%>"><img src="<s:url value="/img/logo_principal2.jpg"/>" width="240" height="60" alt="Jazz Contadores" /></a></div>
                <div id="navigation">
                    <div id="levelOnecontainer">
                        <div style="float: left;">
                            <ul class="levelOne">
                                <li><a href="<%= request.getContextPath()%>" >Inicio</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>
                                <li><a href="<%= request.getContextPath()%>/estudio" >Nuestro estudio</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>
                                <li><a href="<%= request.getContextPath()%>/acceso" class="active" >Acceso</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>
                                <li><a href="<%= request.getContextPath()%>/contacto" >Contáctanos</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div id="fixed_main"> 

            <div id="sesion">
                <%@ include file="/WEB-INF/jspf/barra_sesion_admin.jspf" %>                
            </div>           

            <div id="widthArea">

                <%@ include file="/WEB-INF/jspf/lateral_izq_admin.jspf" %>

                <div id="contentArea">

                    <div id="headerContentArea">
                        <h1 class="medium">Calendario de pagos</h1>
                    </div>                    

                    <div class="pad2">
                        <div>
                            <div id="calendar"></div>
                        </div>                          
                    </div>

                </div>
                <div class="spacer"></div>

            </div>
        </div>

        <!-- footer -->                    
        <%@ include file="/WEB-INF/jspf/footer.jspf" %>        
    </body>
</html>
