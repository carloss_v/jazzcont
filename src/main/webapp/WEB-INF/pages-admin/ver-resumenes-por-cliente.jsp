<%-- 
    Document   : ver-resumenes-por-cliente
    Created on : 05/03/2013, 09:43:14 PM
    Author     : Venegas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>        
        <title>App | Jazz Contadores</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/css/style.css"/>">        
        <link rel="shortcut icon" href="<s:url value="/favicon1.ico"/>">
        <link rel="icon" type="image/ico" href="<s:url value="/favicon1.ico"/>">

        <!-- Botones estilo Google+ -->
        <link type="text/css" href="<s:url value="/css/css3-buttons.css"/>" rel="stylesheet" />

        <link type="text/css" href="<s:url value="/css/custom-theme/jquery-ui-1.9.1.custom.min.css"/>" rel="stylesheet" />	
        <script type="text/javascript" src="<s:url value="/js/jquery-1.7.2.min.js"/>"></script>        
        <script type="text/javascript" src="<s:url value="/js/jquery-ui-1.9.1.custom.min.js"/>"></script> 

        <script type="text/javascript" src="<s:url value="/js/jquery.placeholder.min.js"/>"></script> 
        <script type="text/javascript" src="<s:url value="/js/scripts.js"/>"></script>

        <script type="text/javascript"> 
            $(function() {
                
                // Toggle the dropdown menu's
                $(".dropdown .button").on('click', function () {
                    $(this).parent().find('.dropdown-slider').slideToggle('fast');
                    $(this).find('span.toggle').toggleClass('active');
                    return false;
                });
                
                // Close open dropdown slider/s by clicking elsewhwere on page
                $(document).on('click', function (e) {            
                    if (!$(e.target).is(".dropdown .dropdown-slider a span")) {
                        $('.dropdown-slider').slideUp();
                        $('span.toggle').removeClass('active');
                    }
                }); // END document.bind
                //************** 
                                
                $("#selectResumenMes").change(function() {
                    var selectedOption = $("#selectResumenMes option:selected").val();
                    
                    if(selectedOption == -1) {
                        return;
                    }

                    var urlDestino = "ResumenesAction_show?ruc=<s:property value="ruc" />&periodo=" + selectedOption;
                    window.location = urlDestino;
                });
                
                $("#selectGraficosEst").change(function() {
                    var selectedOption = $("#selectGraficosEst option:selected").val();
                    
                    if(selectedOption == -1) {
                        return;
                    }

                    var urlDestino = "ResumenesAction_showGraphics?ruc=<s:property value="ruc" />&rango=" + selectedOption;
                    window.location = urlDestino;
                });
                                         
            }); 
        </script>

    </head>
    <body>
        <header>
            <div id="header_content">
                <div id="logo"><a href="<%= request.getContextPath()%>"><img src="<s:url value="/img/logo_principal2.jpg"/>" width="240" height="60" alt="Jazz Contadores" /></a></div>
                <div id="navigation">
                    <div id="levelOnecontainer">
                        <div style="float: left;">
                            <ul class="levelOne">
                                <li><a href="<%= request.getContextPath()%>" >Inicio</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>
                                <li><a href="<%= request.getContextPath()%>/estudio" >Nuestro estudio</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>
                                <li><a href="<%= request.getContextPath()%>/acceso" class="active" >Acceso</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>
                                <li><a href="<%= request.getContextPath()%>/contacto" >Contáctanos</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div id="fixed_main"> 

            <div id="sesion">
                <%@ include file="/WEB-INF/jspf/barra_sesion_admin.jspf" %>                
            </div>           

            <div id="widthArea">

                <%@ include file="/WEB-INF/jspf/lateral_izq_admin.jspf" %>

                <div id="contentArea">

                    <%@ include file="/WEB-INF/jspf/header_cliente_sesion_admin.jspf" %>

                    <div id="headerContentArea">
                        <h1 class="medium">Resúmenes por periodos</h1>
                    </div>                    

                    <div id="divR_wrapper">
                        <div class="divR">
                            <a id="resumen1" href="#">Compras, Ventas y Tributos</a>
                            <div class="resumenMeses">
                                <s:select id="selectResumenMes" name="" list="periodosLibros" 
                                          headerKey="-1" headerValue="Seleccione mes"
                                          listKey="key" listValue="value" />
                            </div>
                        </div>
                        <div class="divR">
                            <a id="resumen2" href="#">Gráficos estadísticos</a>
                            <div class="resumenMeses">
                                <select id="selectGraficosEst">
                                    <option value="-1">Seleccione rango</option>                                   
                                    <option value="1">Últimos 3 meses</option>
                                    <option value="2">Últimos 6 meses </option>
                                    <option value="3">Este año</option>                                    
                                </select>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="spacer"></div>

            </div>
        </div>

        <!-- footer -->                    
        <%@ include file="/WEB-INF/jspf/footer.jspf" %>        
    </body>
</html>
