<%-- 
    Document   : ver-resumenes-detallado-por-cliente
    Created on : 26/06/2013, 11:36:25 AM
    Author     : Venegas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>        
        <title>App | Jazz Contadores</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/css/style.css"/>">        
        <link rel="shortcut icon" href="<s:url value="/favicon1.ico"/>">
        <link rel="icon" type="image/ico" href="<s:url value="/favicon1.ico"/>">

        <!-- Botones estilo Google+ -->
        <link type="text/css" href="<s:url value="/css/css3-buttons.css"/>" rel="stylesheet" />

        <link type="text/css" href="<s:url value="/css/custom-theme/jquery-ui-1.9.1.custom.min.css"/>" rel="stylesheet" />	
        <script type="text/javascript" src="<s:url value="/js/jquery-1.7.2.min.js"/>"></script>        
        <script type="text/javascript" src="<s:url value="/js/jquery-ui-1.9.1.custom.min.js"/>"></script> 

        <script type="text/javascript" src="<s:url value="/js/jquery.placeholder.min.js"/>"></script> 
        <script type="text/javascript" src="<s:url value="/js/scripts.js"/>"></script>

        <script type="text/javascript"> 
            $(function() {
                
                // Toggle the dropdown menu's
                $(".dropdown .button").on('click', function () {
                    $(this).parent().find('.dropdown-slider').slideToggle('fast');
                    $(this).find('span.toggle').toggleClass('active');
                    return false;
                });
                
                // Close open dropdown slider/s by clicking elsewhwere on page
                $(document).on('click', function (e) {            
                    if (!$(e.target).is(".dropdown .dropdown-slider a span")) {
                        $('.dropdown-slider').slideUp();
                        $('span.toggle').removeClass('active');
                    }
                }); // END document.bind
                //************** 
                                
            }); 
        </script>

    </head>
    <body>
        <header>
            <div id="header_content">
                <div id="logo"><a href="<%= request.getContextPath()%>"><img src="<s:url value="/img/logo_principal2.jpg"/>" width="240" height="60" alt="Jazz Contadores" /></a></div>
                <div id="navigation">
                    <div id="levelOnecontainer">
                        <div style="float: left;">
                            <ul class="levelOne">
                                <li><a href="<%= request.getContextPath()%>" >Inicio</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>
                                <li><a href="<%= request.getContextPath()%>/estudio" >Nuestro estudio</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>
                                <li><a href="<%= request.getContextPath()%>/acceso" class="active" >Acceso</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>
                                <li><a href="<%= request.getContextPath()%>/contacto" >Contáctanos</a><img class="menue_seperator" src="<s:url value="/img/seperator.jpg"/>" width="2" height="2" /></li>                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div id="fixed_main"> 

            <div id="sesion">
                <%@ include file="/WEB-INF/jspf/barra_sesion_admin.jspf" %>                
            </div>           

            <div id="widthArea">

                <%@ include file="/WEB-INF/jspf/lateral_izq_admin.jspf" %>

                <div id="contentArea">

                    <%@ include file="/WEB-INF/jspf/header_cliente_sesion_admin.jspf" %>

                    <div id="headerContentArea">
                        <h1 class="medium">Resumen del mes: <strong><s:property value="periodo" /></strong></h1>
                    </div>                    

                    <div class="pad2">
                        <h2 class="little">Compras</h2>
                        <div class="detalleResumen1">
                            <span class="idx1">Número de compras:</span>
                            <span class="idx2"><s:property value="numeroCompras" /></span>
                        </div>
                        <div class="detalleResumen1">
                            <div class="idx1">
                                <span>Adquisiciones gravadas destinadas a operaciones gravadas y/o de exportación</span>
                            </div> 
                            <div class="pad1">
                                <span class="idx1">Bases imponibles:</span><span class="idx2"><s:property value="basesImponible1" /></span><br />
                                <span class="idx1">IGVs:</span><span class="idx2"><s:property value="igvs1" /></span>
                            </div>                        
                        </div>
                        <div class="detalleResumen1">
                            <div class="idx1">
                                <span>Adquisiciones gravadas destinadas a operaciones gravadas y/o de exportación y a operaciones no gravadas</span>
                            </div>
                            <div class="pad1">
                                <span class="idx1">Bases imponibles:</span><span class="idx2"><s:property value="basesImponible2" /></span><br />
                                <span class="idx1">IGVs:</span><span class="idx2"><s:property value="igvs2" /></span>
                            </div> 
                        </div>
                        <div class="detalleResumen1">
                            <div class="idx1">
                                <span>Adquisiciones gravadas destinadas a operaciones no gravadas</span>
                            </div>
                            <div class="pad1">
                                <span class="idx1">Bases imponibles:</span><span class="idx2"><s:property value="basesImponible3" /></span><br />
                                <span class="idx1">IGVs:</span><span class="idx2"><s:property value="igvs3" /></span>
                            </div> 
                        </div>                        
                        <div class="detalleResumen1">
                            <span class="idx1">ISC</span>
                            <span class="idx2"><s:property value="iscC" /></span>
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Otros tributos y cargos</span>
                            <span class="idx2"><s:property value="otrosTributosC" /></span>                                          
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Total adquisiciones no gravadas:</span>
                            <span class="idx2"><s:property value="valorAdquisicionesNoGravadas" /></span>                                         
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Total de adquisiciones gravadas:</span>
                            <span class="idx2"><s:property value="valorAdquisicionesGravadas" /></span>                                         
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Importe total de compras:</span>
                            <span class="idx2"><s:property value="importeTotalC" /></span>                                           
                        </div>
                    </div> 

                    <div class="pad2">
                        <h2 class="little">Ventas</h2>
                        <div class="detalleResumen1">
                            <span class="idx1">Número de ventas:</span>
                            <span class="idx2"><s:property value="numeroVentas" /></span>
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Valor facturado de exportaciones:</span>
                            <span class="idx2"><s:property value="valorFacturadoExportaciones" /></span>                                           
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Base imponible de las operaciones gravadas:</span>
                            <span class="idx2"><s:property value="baseImponibleOperacionesGravadas" /></span>                                           
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Importe total de las operaciones exoneradas:</span>
                            <span class="idx2"><s:property value="importeTotalOperacionesExoneradas" /></span>                                           
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Importe total de las operaciones inafectas:</span>
                            <span class="idx2"><s:property value="importeTotalOperacionesInafectas" /></span>                                           
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">ISC:</span>
                            <span class="idx2"><s:property value="iscV" /></span>                                           
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">IGVs y/o IPMs:</span>
                            <span class="idx2"><s:property value="igvsV" /></span>                                           
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Otros tributos y cargos que no forman parte de la base imponible:</span>
                            <span class="idx2"><s:property value="otrosTributosV" /></span>                                           
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Total de las operaciones gravadas:</span>
                            <span class="idx2"><s:property value="valorAdquisicionesGravadasV" /></span>                                           
                        </div>
                        <div class="detalleResumen1">                        
                            <span class="idx1">Total de las operaciones no gravadas:</span>
                            <span class="idx2"><s:property value="valorAdquisicionesNoGravadasV" /></span>                                           
                        </div>                        
                        <div class="detalleResumen1">                        
                            <span class="idx1">Importe total de ventas:</span>
                            <span class="idx2"><s:property value="importeTotalV" /></span>                                           
                        </div>                                              
                    </div>

                    <div class="pad2">
                        <h2 class="little">Monto a tributar</h2>
                        <div class="detalleResumen1">

                            <s:if test="%{empresaCliente.getTipoRegimen() == 'NRUS'}">
                                <span>Cuota fija para categoría <s:property value="empresaCliente.categoriaNRUS" /> de NRUS</span><br />
                                <s:if test="%{empresaCliente.getCategoriaNRUS() == 1}">
                                    <span class="idx1">Cuota mensual:</span>
                                    <span class="idx2"><s:property value="tributo" /></span>
                                </s:if>
                                <s:if test="%{empresaCliente.getCategoriaNRUS() == 2}">
                                    <span class="idx1">Cuota mensual:</span>
                                    <span class="idx2"><s:property value="tributo" /></span>
                                </s:if>
                                <s:if test="%{empresaCliente.getCategoriaNRUS() == 3}">
                                    <span class="idx1">Cuota mensual:</span>
                                    <span class="idx2"><s:property value="tributo" /></span>
                                </s:if>
                                <s:if test="%{empresaCliente.getCategoriaNRUS() == 4}">
                                    <span class="idx1">Cuota mensual:</span>
                                    <span class="idx2"><s:property value="tributo" /></span>
                                </s:if>
                                <s:if test="%{empresaCliente.getCategoriaNRUS() == 5}">
                                    <span class="idx1">Cuota mensual:</span>
                                    <span class="idx2"><s:property value="tributo" /></span>
                                </s:if>                                
                            </s:if>

                            <s:if test="%{empresaCliente.getTipoRegimen() == 'RER'}">
                                <span class="idx1">Impuesto a la renta:</span>
                                <span class="idx2"><s:property value="impuestoRenta" /></span><br />
                                <span class="idx1">IGV e Impuesto de Promoción Municipal:</span>
                                <span class="idx2"><s:property value="impuestoIgv" /></span>
                            </s:if>
                            <s:if test="%{empresaCliente.getTipoRegimen() == 'RG'}">
                                <span class="idx1">Tributo:</span>
                                <span class="idx2"><s:property value="tributo" /></span>
                            </s:if>
                        </div> 
                        
                    </div>

                </div>
                <div class="spacer"></div>

            </div>
        </div>

        <!-- footer -->                    
        <%@ include file="/WEB-INF/jspf/footer.jspf" %>        
    </body>
</html>
