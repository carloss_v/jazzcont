Ext.require('Ext.chart.*');
Ext.require(['Ext.Window', 'Ext.layout.container.Fit', 'Ext.fx.target.Sprite', 'Ext.window.MessageBox']);

Ext.onReady(function() {

    Ext.define('Ventas', {
        extend: 'Ext.data.Model',
        fields: [
        {
            name: 'periodo', 
            type: 'auto'
        },
        {
            name: 'total', 
            type: 'auto'
        }           
        ]
    });
    
    var QueryString = function () {
        // This function is anonymous, is executed immediately and 
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = pair[1];
            // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]], pair[1] ];
                query_string[pair[0]] = arr;
            // If third or later entry with this name
            } else {
                query_string[pair[0]].push(pair[1]);
            }
        } 
        return query_string;
    } ();
    
    var store = Ext.create('Ext.data.Store', {
        model: 'Ventas',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url : 'LibrosAjaxAction_listTotalVentasPorPeriodo?ruc=' + QueryString.ruc + '&rango=' + QueryString.rango,
            reader: {
                type: 'json'                
            }
        }   
    });

    Ext.create('Ext.chart.Chart', {
        store: store,        
        width: 500,
        height: 300,
        animate: true,
        renderTo: 'graficoVentas1',
        axes: [
        {
            title: 'Totales ventas',
            type: 'Numeric',
            position: 'left',
            fields: ['total'],
            grid: true,
            minimum: 0
            //adjustMinimumByMajorUnit: 0
        },
        {
            title: 'Periodos',
            type: 'Category',
            position: 'bottom',
            fields: ['periodo']            
        }
        ],
        series: [
        {
            type: 'column',
            axis: 'left',
            highlight: true,
            tips: {
                trackMouse: true,
                width: 140,
                height: 28,
                renderer: function(storeItem, item) {
                    this.setTitle(storeItem.get('periodo') + ': S/.' + storeItem.get('total'));
                }
            },
            label: {
                display: 'insideEnd',
                'text-anchor': 'middle',
                field: 'total',
                renderer: Ext.util.Format.numberRenderer('0.00'),
                orientation: 'vertical',
                color: '#333'
            },
            xField: 'periodo',
            yField: 'total'
        }
        ]
    });    

});