Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*',
    'Ext.toolbar.Paging',
    'Ext.ModelManager'    
    ]);
    
Ext.onReady(function() {
       
    Ext.define('AsientoContable', {
        extend: 'Ext.data.Model',
        fields: []
    });

    var QueryString = function () {
        // This function is anonymous, is executed immediately and 
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = pair[1];
            // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]], pair[1] ];
                query_string[pair[0]] = arr;
            // If third or later entry with this name
            } else {
                query_string[pair[0]].push(pair[1]);
            }
        } 
        return query_string;
    } ();

    var store = Ext.create('Ext.data.Store', {
        model: 'AsientoContable',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url : 'LibrosAjaxAction_listAsientosContables?idLibro=' + QueryString.idLibro,
            reader: {
                type: 'json',
                root: 'lineasDiarioSimplificado',
                totalProperty: 'totalCount'
            }
        },
        listeners: {
            'metachange': function(store, meta) {
                grid.reconfigure(store, meta.columns);
            }
        }
    });
    
    
    // create the Grid
    var grid = Ext.create('Ext.grid.Panel', {
        store: store,
        columnLines: true,
        columns: [],
        height: 550,
        width: 840,
        features: [{
            ftype: 'summary'
        }],
        title: 'Libro Diario de Formato Simplificado ' + QueryString.prd,        
        renderTo: 'libroDSExt'
    });   
});